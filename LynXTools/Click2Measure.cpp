#include "../LXCommonHeader.h"
#include "../LXConfig.h"
#include "../LXCamera.h"
#include "../LXService.h"

LxConf gloconf;
time_t LxTimeServ::stime=0;
int sta=0;
Point2d ll,rr;
LxStereoCam scam(0,1);
void onMouse1(int event,int x,int y,int,void*)
{
  if (sta==1&&event==CV_EVENT_LBUTTONDOWN)
  {
    ll=Point2d(x,y);
    printf("%lf %lf\n",ll.x,ll.y);
    sta=2;
  }
}

void onMouse2(int event,int x,int y,int,void*)
{
  if (sta==2&&event==CV_EVENT_LBUTTONDOWN)
  {
    rr=Point2d(x,y);
    printf("%lf %lf\n",rr.x,rr.y);
    if (abs(ll.y-rr.y)<10)
    {
      Point3d p3d;
      p3d=scam.reproject(ll,ll.x-rr.x);
      printf("%lf %lf %lf\n\n",p3d.x,p3d.y,p3d.z);
    }
    sta=1;
  }
}

int main()
{
  LxTimeServ::start();
  LxRandServ::start();
  gloconf.load("../LynXData/settings.xml");
  //scam=LxStereoCam(gloconf.cam_dev_id[0],gloconf.cam_dev_id[1]);
  scam.init("../LynXData/CamMat1.xml","../LynXData/CamMat2.xml","../LynXData/MonoData.xml");
  namedWindow("left",1);
  namedWindow("right",1);
  setMouseCallback("left",onMouse1,0);
  setMouseCallback("right",onMouse2,0);
  LxDtimer timer1;
  Mat frame1,frame2;
  scam.refresh(frame1,frame2);
  printf("press z to hold\n");
  for (;;)
  {
    scam.refresh(frame1,frame2);
    imshow("left",frame1);
    imshow("right",frame2);
    int wk1,wk;
    if ((wk1=waitKey(100))==KEY_Z)
    {
      sta=1;
      printf("holding,you can click,press z to release\n");
      for (;;)
      {
        if ((wk=waitKey(5))==KEY_Z)
        {
          sta=0;
          printf("press z to hold\n");
          break;
        }
        else
          if (wk>0)
          {
            destroyWindow("left");
            destroyWindow("right");
            cout<<"We running for "<<(LxTimeServ::uptime())<<" seconds"<<endl;
            return 0;
          }
      }
    }
    else
      if (wk1>0)
        break;
  }
  destroyWindow("left");
  destroyWindow("right");
  cout<<"We running for "<<(LxTimeServ::uptime())<<" seconds"<<endl;
  return 0;
}
