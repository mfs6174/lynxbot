#include "../LXCommonHeader.h"

const string SWAPPATH="/dev/shm/";

int main(int argc, char* argv[])
{
  Mat f1,f2,frame;
  VideoCapture cap;
  cap.open(atoi(argv[1]));
  cap.set(CV_CAP_PROP_FRAME_WIDTH,1600);
  cap.set(CV_CAP_PROP_FRAME_HEIGHT,1200);
  cap.grab();
  if (!cap.retrieve(frame))
    cout<<"capture fail"<<endl;
  f1=frame.clone();
  cap.open(atoi(argv[2]));
  cap.set(CV_CAP_PROP_FRAME_WIDTH,1600);
  cap.set(CV_CAP_PROP_FRAME_HEIGHT,1200);
  cap.grab();
  if (!  cap.retrieve(frame))
    cout<<"capture fail"<<endl;
  f2=frame.clone();
  cap.release();
  resize(f1,f1,Size(800,600));
  resize(f2,f2,Size(800,600));
  IplImage img1=f1,img2=f2;
  cvSaveImage((SWAPPATH+"cam1.pgm").c_str(),&img1);
  cvSaveImage((SWAPPATH+"cam2.pgm").c_str(),&img2);
  return 0;
}
  
  
  
