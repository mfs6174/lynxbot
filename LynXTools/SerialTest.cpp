#include "../LXCommonHeader.h"
#include "../LXConfig.h"
#include "../LXService.h"
#include "../LXController.h"

time_t LxTimeServ::stime=0;
LxController ctl;

int main()
{
  int od,tm;
  if (ctl.connect("/dev/ttyUSB0"))
    return -1;
  while (cin>>od>>tm)
  {
    switch (od)
    {
    case 1:
      ctl.moveFwd(tm);
      break;
    case 2:
      ctl.moveBwd(tm);
      break;
    case 3:
      ctl.turnRt(tm);
      break;
    case 4:
      ctl.turnLt(tm);
      break;
    case 0:
      ctl.shutdown();
      return 0;
    }
  }
  return 0;
}
  
