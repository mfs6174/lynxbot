#include "../LXCommonHeader.h"
#include "../LXConfig.h"
#include "../LXCamera.h"
#include "../LXService.h"

LxConf gloconf;
time_t LxTimeServ::stime=0;
const int SQLEN=28;

//
// Given a list of chessboard images, the number of corners (nx, ny)
// on the chessboards, and a flag: useCalibrated for calibrated (0) or
// uncalibrated (1: use cvStereoCalibrate(), 2: compute fundamental
// matrix separately) stereo. Calibrate the cameras and display the
// rectified results along with the computed disparity images.
//
static void StereoCalib(const string cam1, const string cam2,int nx, int ny, int useUncalibrated,int imnm)
{
  int displayCorners = 1;
  int showUndistorted = 1;
  bool isVerticalStereo = false;//OpenCV can handle left-right
  //or up-down camera arrangements
  const int maxScale = 1;
  const float squareSize = SQLEN; //Set this to your actual square size
  int i, j, lr, nframes, n = nx*ny, N = 0;
  vector<string> imageNames[2];
  vector<CvPoint3D32f> objectPoints;
  vector<CvPoint2D32f> points[2];
  vector<int> npoints;
  //vector<uchar> active[2];
  vector<CvPoint2D32f> temp[2]={vector<CvPoint2D32f>(n),vector<CvPoint2D32f>(n)};
  CvSize imageSize = {0,0};
  // ARRAY AND VECTOR STORAGE:
  // double M1[3][3], M2[3][3], D1[5], D2[5];
  double R[3][3], T[3], E[3][3], F[3][3];
  //CvMat _M1 = cvMat(3, 3, CV_64F, M1 );
  //CvMat _M2 = cvMat(3, 3, CV_64F, M2 );
  //CvMat _D1 = cvMat(1, 5, CV_64F, D1 );
  //CvMat _D2 = cvMat(1, 5, CV_64F, D2 );
  CvMat _R = cvMat(3, 3, CV_64F, R );
  CvMat _T = cvMat(3, 1, CV_64F, T );
  CvMat _E = cvMat(3, 3, CV_64F, E );
  CvMat _F = cvMat(3, 3, CV_64F, F );
  cvNamedWindow( "corners", 1 );
  cvNamedWindow( "video left", 1 );
  cvNamedWindow( "video right", 1 );
  // READ IN THE LIST OF CHESSBOARDS:
  LxStereoCam scam(0,1);
  Mat frame[2],gf[2];
  IplImage* img;//=cvCreateImage( imageSize, IPL_DEPTH_8U, 1 );
  for(i=1;i<=imnm;)
  {
    scam.refresh(frame[0],frame[1]);
    //cout<<frame[0].size().width<<endl;
    imshow("video left",frame[0]);
    imshow("video right",frame[1]);
    string cfm[2]={"left","right"};
    int key=waitKey(255);
    if (key==KEY_ESC)
      exit(-1);
    if(key>0)//Press any key to shot
    {
      cout<<"shot"<<endl;
      bool succ=true;
      for (lr=0;lr<=1;lr++)
      {
        cvtColor(frame[lr],gf[lr],CV_RGB2GRAY);
        IplImage tempimg=gf[lr];
        img=&tempimg;
        int count = 0, result=0;
        imageSize = cvGetSize(img);
        //FIND CHESSBOARDS AND CORNERS THEREIN:
        for( int s = 1; s <= maxScale; s++ )
        {
          IplImage* timg = img;
          if( s > 1 )
          {
            timg = cvCreateImage(cvSize(img->width*s,img->height*s),
                                 img->depth, img->nChannels );
            cvResize( img, timg, CV_INTER_CUBIC );
          }
          result = cvFindChessboardCorners( timg, cvSize(nx, ny),
                                            &temp[lr][0], &count,
                                            CV_CALIB_CB_ADAPTIVE_THRESH |CV_CALIB_CB_FILTER_QUADS|
                                            CV_CALIB_CB_NORMALIZE_IMAGE);
          if( timg != img )
            cvReleaseImage( &timg );
          if( result || s == maxScale )
            for( j = 0; j < count; j++ )
            {
              temp[lr][j].x /= s;
              temp[lr][j].y /= s;
            }
          if( result )
            break;
        }
        IplImage* cimg = cvCreateImage( imageSize, 8, 3 );
        cvCvtColor( img, cimg, CV_GRAY2BGR );
        cvDrawChessboardCorners( cimg, cvSize(nx, ny), &temp[lr][0],
                                 count, result );
        cvShowImage( "corners", cimg );
        cvReleaseImage( &cimg );
        if ( (count!=n)|| (!succ) )
        {
          cout<<"Fail"<<endl;
          succ=false;
          break;
        }
        else
        {
          char cc=cvWaitKey(0);
          if( cc == KEY_ESC ) //Allow ESC to quit
            exit(-1);
          if (cc != 'z')
          {
            cout<<cc<<endl;
            cout<<"Drop"<<endl;
            succ=false;
            break;
          }
          //active[lr].push_back((uchar)result);
          //assert( result != 0 );
          //Calibration will suffer without subpixel interpolation
          cvFindCornerSubPix( img, &temp[lr][0], count,
                              cvSize(11, 11), cvSize(-1,-1),
                              cvTermCriteria(CV_TERMCRIT_ITER+CV_TERMCRIT_EPS,
                                             30, 0.01) );
          cout<<cfm[lr]+" confirmed"<<endl;
        }
      }
      if (succ)
      {
        cout<<"Collect No."<<i<<endl;
        for (int lll=0;lll<=1;lll++)
        {
          vector<CvPoint2D32f>& pts = points[lll];
          N = pts.size();
          pts.resize(N + n, cvPoint2D32f(0,0));
          copy( temp[lll].begin(), temp[lll].end(), pts.begin() + N );
        }
        stringstream id;
        id<<i;
        IplImage p1=gf[0];
        IplImage p2=gf[1];
        cvSaveImage( ("left_"+id.str()+".pgm" ).c_str(),&p1);
        cvSaveImage( ("right_"+id.str()+".pgm" ).c_str(),&p2);
        i++;
      }
      else
        cout<<"Please reshot now!"<<endl;
    }
  }
  
  printf("\n");
  // HARVEST CHESSBOARD 3D OBJECT POINT LIST:
  nframes = imnm;//Number of good chessboads found
  objectPoints.resize(nframes*n);
  for( i = 0; i < ny; i++ )
    for( j = 0; j < nx; j++ )
      objectPoints[i*nx + j] =
        cvPoint3D32f(i*squareSize, j*squareSize, 0);
  for( i = 1; i < nframes; i++ )
    copy( objectPoints.begin(), objectPoints.begin() + n,
          objectPoints.begin() + i*n );
  npoints.resize(nframes,n);
  N = nframes*n;
  CvMat _objectPoints = cvMat(1, N, CV_32FC3, &objectPoints[0] );
  CvMat _imagePoints1 = cvMat(1, N, CV_32FC2, &points[0][0] );
  CvMat _imagePoints2 = cvMat(1, N, CV_32FC2, &points[1][0] );
  CvMat _npoints = cvMat(1, npoints.size(), CV_32S, &npoints[0] );
  // cvSetIdentity(&_M1);
  // cvSetIdentity(&_M2);
  // cvZero(&_D1);
  // cvZero(&_D2);
  Mat cm1,cm2,dist1,dist2;
  FileStorage fs(cam1,FileStorage::READ);
  fs["Intrinsics"]>>cm1;
  fs["Distortion"]>>dist1;
  fs.open(cam2,FileStorage::READ);
  fs["Intrinsics"]>>cm2;
  fs["Distortion"]>>dist2;
  CvMat _M1 = cm1;
  CvMat _M2 = cm2;
  CvMat _D1 = dist1;
  CvMat _D2 = dist2;
  fs.open("stecalidata.xml",FileStorage::WRITE);
  // CALIBRATE THE STEREO CAMERAS
  printf("Running stereo calibration ...");
  fflush(stdout);
  double serror=cvStereoCalibrate( &_objectPoints, &_imagePoints1,
                     &_imagePoints2, &_npoints,
                     &_M1, &_D1, &_M2, &_D2,
                     imageSize, &_R, &_T, &_E, &_F,
                                   cvTermCriteria(CV_TERMCRIT_ITER+
                                    CV_TERMCRIT_EPS, 200, 1e-6),
                                   CV_CALIB_FIX_INTRINSIC );
  printf(" done with error=%.6lf and T=%.2lf\n",serror,T[0]);
  fs<<"R"<<cvarrToMat(&_R)<<"T"<<cvarrToMat(&_T)<<"Essential"<<cvarrToMat(&_E)<<"Fundamental"<<cvarrToMat(&_F);
  // CALIBRATION QUALITY CHECK
  // because the output fundamental matrix implicitly
  // includes all the output information,
  // we can check the quality of calibration using the
  // epipolar geometry constraint: m2^t*F*m1=0
  vector<CvPoint3D32f> lines[2];
  points[0].resize(N);
  points[1].resize(N);
  _imagePoints1 = cvMat(1, N, CV_32FC2, &points[0][0] );
  _imagePoints2 = cvMat(1, N, CV_32FC2, &points[1][0] );
  lines[0].resize(N);
  lines[1].resize(N);
  CvMat _L1 = cvMat(1, N, CV_32FC3, &lines[0][0]);
  CvMat _L2 = cvMat(1, N, CV_32FC3, &lines[1][0]);
  //Always work in undistorted space
  cvUndistortPoints( &_imagePoints1, &_imagePoints1,
                     &_M1, &_D1, 0, &_M1 );
  cvUndistortPoints( &_imagePoints2, &_imagePoints2,
                     &_M2, &_D2, 0, &_M2 );
  cvComputeCorrespondEpilines( &_imagePoints1, 1, &_F, &_L1 );
  cvComputeCorrespondEpilines( &_imagePoints2, 2, &_F, &_L2 );
  double avgErr = 0;
  for( i = 0; i < N; i++ )
  {
    double err = fabs(points[0][i].x*lines[1][i].x +
                      points[0][i].y*lines[1][i].y + lines[1][i].z)
      + fabs(points[1][i].x*lines[0][i].x +
             points[1][i].y*lines[0][i].y + lines[0][i].z);
    avgErr += err;
  }
  printf( "avg err = %g\n", avgErr/(nframes*n) );
  //COMPUTE AND DISPLAY RECTIFICATION
  if( showUndistorted )
  {
    CvMat* mx1 = cvCreateMat( imageSize.height,
                              imageSize.width, CV_32F );
    CvMat* my1 = cvCreateMat( imageSize.height,
                              imageSize.width, CV_32F );
    CvMat* mx2 = cvCreateMat( imageSize.height,

                              imageSize.width, CV_32F );
    CvMat* my2 = cvCreateMat( imageSize.height,
                              imageSize.width, CV_32F );
    CvMat* img1r = cvCreateMat( imageSize.height,
                                imageSize.width, CV_8U );
    CvMat* img2r = cvCreateMat( imageSize.height,
                                imageSize.width, CV_8U );
    CvMat* disp = cvCreateMat( imageSize.height,
                               imageSize.width, CV_16S );
    CvMat* vdisp = cvCreateMat( imageSize.height,
                                imageSize.width, CV_8U );
    CvMat* pair;
    double R1[3][3], R2[3][3], P1[3][4], P2[3][4],Q[4][4];
    CvMat _R1 = cvMat(3, 3, CV_64F, R1);
    CvMat _R2 = cvMat(3, 3, CV_64F, R2);
    CvMat _Q = cvMat(4, 4, CV_64F, Q);
    // IF BY CALIBRATED (BOUGUET'S METHOD)
    if( useUncalibrated == 0 )
    {
      CvMat _P1 = cvMat(3, 4, CV_64F, P1);
      CvMat _P2 = cvMat(3, 4, CV_64F, P2);
      cvStereoRectify( &_M1, &_M2, &_D1, &_D2, imageSize,
                       &_R, &_T,
                       &_R1, &_R2, &_P1, &_P2, &_Q,
                       0/*CV_CALIB_ZERO_DISPARITY*/ ,0);
      isVerticalStereo = fabs(P2[1][3]) > fabs(P2[0][3]);
      fs<<"R1"<<cvarrToMat(&_R1)<<"R2"<<cvarrToMat(&_R2)<<"P1"<<cvarrToMat(&_P1)<<"P2"<<cvarrToMat(&_P2)<<"Q"<<cvarrToMat(&_Q);
      //Precompute maps for cvRemap()
      cvInitUndistortRectifyMap(&_M1,&_D1,&_R1,&_P1,mx1,my1);
      cvInitUndistortRectifyMap(&_M2,&_D2,&_R2,&_P2,mx2,my2);
    }
    //OR ELSE HARTLEY'S METHOD
    else if( useUncalibrated == 1 || useUncalibrated == 2 )
      // use intrinsic parameters of each camera, but
      // compute the rectification transformation directly
      // from the fundamental matrix
    {
      double H1[3][3], H2[3][3], iM[3][3];
      CvMat _H1 = cvMat(3, 3, CV_64F, H1);
      CvMat _H2 = cvMat(3, 3, CV_64F, H2);
      CvMat _iM = cvMat(3, 3, CV_64F, iM);
      //Just to show you could have independently used F
      if( useUncalibrated == 2 )
        cvFindFundamentalMat( &_imagePoints1,
                              &_imagePoints2, &_F);
      cvStereoRectifyUncalibrated( &_imagePoints1,
                                   &_imagePoints2, &_F,
                                   imageSize,
                                   &_H1, &_H2, 3);
      cvInvert(&_M1, &_iM);
      cvMatMul(&_H1, &_M1, &_R1);
      cvMatMul(&_iM, &_R1, &_R1);
      cvInvert(&_M2, &_iM);
      cvMatMul(&_H2, &_M2, &_R2);
      cvMatMul(&_iM, &_R2, &_R2);
      //Precompute map for cvRemap()
      cvInitUndistortRectifyMap(&_M1,&_D1,&_R1,&_M1,mx1,my1);

      cvInitUndistortRectifyMap(&_M2,&_D1,&_R2,&_M2,mx2,my2);
    }
    else
      assert(0);
    cvNamedWindow( "rectified", 1 );
    // RECTIFY THE IMAGES AND FIND DISPARITY MAPS
    if( !isVerticalStereo )
      pair = cvCreateMat( imageSize.height, imageSize.width*2,
                          CV_8UC3 );
    else
      pair = cvCreateMat( imageSize.height*2, imageSize.width,
                          CV_8UC3 );
    //Setup for finding stereo corrrespondences
    CvStereoBMState *BMState = cvCreateStereoBMState();
    assert(BMState != 0);
    BMState->preFilterSize=41;
    BMState->preFilterCap=31;
    BMState->SADWindowSize=41;
    BMState->minDisparity=-64;
    BMState->numberOfDisparities=128;
    BMState->textureThreshold=10;
    BMState->uniquenessRatio=15;
    IplImage* img1;
    IplImage* img2;
    for( ; ; )
    {
      scam.refresh(frame[0],frame[1]);
      for (lr=0;lr<=1;lr++)
        cvtColor(frame[lr],gf[lr],CV_RGB2GRAY);
      IplImage tempimg1=gf[0],tempimg2=gf[1];
      img1=&tempimg1;
      img2=&tempimg2;
      //IplImage* img1=cvLoadImage(imageNames[0][i].c_str(),0);
      //IplImage* img2=cvLoadImage(imageNames[1][i].c_str(),0);
      if( img1 && img2 )
      {
        CvMat part;
        cvRemap( img1, img1r, mx1, my1 );
        cvRemap( img2, img2r, mx2, my2 );
        if( !isVerticalStereo || useUncalibrated != 0 )
        {
          // When the stereo camera is oriented vertically,
          // useUncalibrated==0 does not transpose the
          // image, so the epipolar lines in the rectified
          // images are vertical. Stereo correspondence
          // function does not support such a case.
          cvFindStereoCorrespondenceBM( img1r, img2r, disp,
                                        BMState);
          cvNormalize( disp, vdisp, 0, 256, CV_MINMAX );
          cvNamedWindow( "disparity" );
          cvShowImage( "disparity", vdisp );
        }
        if( !isVerticalStereo )
        {
          cvGetCols( pair, &part, 0, imageSize.width );
          cvCvtColor( img1r, &part, CV_GRAY2BGR );
          cvGetCols( pair, &part, imageSize.width,
                     imageSize.width*2 );
          cvCvtColor( img2r, &part, CV_GRAY2BGR );
          for( j = 0; j < imageSize.height; j += 16 )
            cvLine( pair, cvPoint(0,j),
                    cvPoint(imageSize.width*2,j),
                    CV_RGB(0,255,0));
        }
        else
        {
          cvGetRows( pair, &part, 0, imageSize.height );
          cvCvtColor( img1r, &part, CV_GRAY2BGR );
          cvGetRows( pair, &part, imageSize.height,
                     imageSize.height*2 );
          cvCvtColor( img2r, &part, CV_GRAY2BGR );
          for( j = 0; j < imageSize.width; j += 16 )
            cvLine( pair, cvPoint(j,0),
                    cvPoint(j,imageSize.height*2),
                    CV_RGB(0,255,0));
        }
        cvShowImage( "rectified", pair );
        if( cvWaitKey(5) == KEY_ESC )
          break;
      }
      //cvReleaseImage( &img1 );
      //cvReleaseImage( &img2 );
    }
    cvReleaseStereoBMState(&BMState);
    cvReleaseMat( &mx1 );
    cvReleaseMat( &my1 );
    cvReleaseMat( &mx2 );
    cvReleaseMat( &my2 );
    cvReleaseMat( &img1r );
    cvReleaseMat( &img2r );
    cvReleaseMat( &disp );
  }
}
int main(int argc,char* argv[])
{
  LxTimeServ::start();
  LxRandServ::start();
  gloconf.load("../LynXData/settings.xml");
  StereoCalib("../LynXData/CamMat1.xml","../LynXData/CamMat2.xml",atoi(argv[1]), atoi(argv[2]), 0,atoi(argv[3]) );
  return 0;
}
