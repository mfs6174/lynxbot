#ifndef LXPCLOUD_H
#define LXPCLOUD_H

#define X_LEN 50
#define Z_LEN 50
#define OIDX 60

struct LxPointCLoud
{
  int xLen,zLen;
  double gridSize;
  int xOffset,zOffset;
  bool mapValid[200][200];
  double mapDEM[20000];
  int mapX[20000],mapZ[20000];
  int validFin;
  double lat;
  LxPointCLoud(int gdsize);
  int acceptData(const Mat & disp,const LxGMTrans &cam2bot,const LxGMTrans &bot2grd,const LxStereoCam *scam);
private:
  vector<double> mapPnt[200][200];
  Point validIdx[20000];
  int validCnt;
  int  _getCoord(double x);
  int _Pc2DEM(vector<double> &vec,double &fin);
};
  

#endif
