#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXService.h"
#include "LXCamera.h"
#include "LXGeometry.h"
#include "LXPCloud.h"

extern LxConf gloconf;

LxPointCLoud::LxPointCLoud(int gdsize)
{
  xLen=X_LEN;
  zLen=Z_LEN;
  gridSize=gdsize;
  memset(mapValid,0,sizeof(mapValid));
  validCnt=validFin=0;
}

int LxPointCLoud::_getCoord(double x)
{
  double t=x/gridSize;
  int i=(int)t;
  double f=t-i;
  int p;
  if (abs(f)<=0.5)
    p=0;
  else
    p=1;
  if (t<0)
    p=-p;
  return i+p;
}

int LxPointCLoud::_Pc2DEM(vector<double> &vec,double &fin)
{
  sort(vec.begin(),vec.end());
  int i=0;
  double dth=gloconf.cl_lyr_len/2.0;
  while (lat-vec[i]>dth&&i<vec.size()) i++;
  bool findBtm=false;
  int pntPerLayer=max((int)(vec.size()/(3000/gloconf.cl_lyr_len)),gloconf.cl_pnpl_hth);
  int sly=-1,cly=0,lycnt=0;
  if (i<vec.size())
  {
    if (abs(lat-vec[i])<=dth)
    {
      for (int j=i;j<vec.size();j++)
      {
        if (vec[j]>50000)
          break;
        if (vec[j]>=lat+cly*gloconf.cl_lyr_len+dth)
        {
          if (lycnt<pntPerLayer)
          {
            sly=cly-1;
            break;
          }
          else
          {
            sly=cly;
            cly++;
            lycnt=1;
          }
        }
        else
        {
          lycnt++;
        }
      }
    }
    else
      return -1;
  }
  else
  {
    findBtm=true;
  }

  if (findBtm||sly<0)
  {
    lycnt=0;cly=0;
    double sol=0,sth;
    int j=0;
    while (vec[j]<50000&&j<vec.size()) j++;
    sth=vec[j];
    for (;j<vec.size();j++)
    {
      if (vec[j]-sth>gloconf.cl_lyr_len)
      {
        if (lycnt>=pntPerLayer)
        {
          fin=sol/lycnt;
          break;
        }
        else
        {
          lycnt=1;
          sth=vec[j];
          sol=sth;
        }
      }
      else
      {
        lycnt++;
        sol+=vec[j];
      }
    }
    if (lycnt>=pntPerLayer)
      return 0;
  }
  else
  {
    double sol=0;
    lycnt=0;
    int j=0;
    while (vec[j]<lat+sly*gloconf.cl_lyr_len+dth&&j<vec.size())
      if (vec[j]>=lat+sly*gloconf.cl_lyr_len-dth)
      {
        lycnt++;
        sol+=vec[j];
      }
    fin=sol/lycnt;
    return 0;
  }
  return -1;

  // //change to a new algorithm
  // double sol=0,sth;
  // double firstH=0,lastH=0;
  // int j=0;
  // bool isStart=false;
  // int layerI=-1,firstP=-1;
  // double p1,p2;
  // while (vec[j]<-50000&&j<vec.size()) j++;
  // sth=vec[j];
  // for (;j<vec.size();j++)
  // {
  //   if (vec[j]>50000)
  //     break;
  //   if (vec[j]-sth>=gloconf.cl_lyr_len)
  //   {
  //     if (lycnt>=pntPerLayer)
  //     {
  //       layerI++;
  //       p2=sol/lycnt;
  //       lastH=vec[j-1];
  //       if (!isStart)
  //       {
  //         isStart=true;
  //         firstH=sth;
  //         firstP=j-1;
  //         p1=sol/lycnt;
  //       }
  //     }
  //     else
  //     {
  //       if (isStart)
  //         break;
  //     }
  //     lycnt=1;
  //     sth=vec[j];
  //     sol=sth;
  //   }
  //   else
  //   {
  //     lycnt++;
  //     sol+=vec[j];
  //   }
  // }
  // if (lycnt>=pntPerLayer)
  // {
  //   layerI++;
  //   p2=sol/lycnt;
  //   lastH=vec[j-1];
  //   if (!isStart)
  //   {
  //     isStart=true;
  //     firstH=sth;
  //     firstP=j;
  //     p1=sol/lycnt;
  //   }  
  // }
  // if (isStart)
  // {
  //   if (lat-firstH>
  // }
  // return -1;
}


int LxPointCLoud::acceptData(const Mat & disp,const LxGMTrans &cam2bot,const LxGMTrans &bot2grd,const LxStereoCam *scam)
{
  memset(mapValid,0,sizeof(mapValid));
  xOffset=_getCoord(bot2grd.getLoc().x);
  zOffset=_getCoord(bot2grd.getLoc().z);
  lat=_getCoord(bot2grd.getLoc().y);
  validCnt=0;
  for (int y=0;y<disp.rows;y++)
    for (int x=0;x<disp.cols;x++)
    {
      if (disp.at<float>(y,x)>0)
      {
        Point3d cpnt;
        cpnt=scam->reproject(Point2d(x,y),disp.at<float>(y,x));
        cpnt=cam2bot.trans(cpnt);
        cpnt=bot2grd.trans(cpnt);
        int gx=_getCoord(cpnt.x)-xOffset+OIDX,gz=_getCoord(cpnt.z)-zOffset+OIDX;
        if ( abs((gx-OIDX))>xLen || abs((gz-OIDX))>zLen)
          continue;
        if (mapValid[gx][gz])
        {
          mapPnt[gx][gz].push_back(cpnt.y);
        }
        else
        {
          mapValid[gx][gz]=true;
          validCnt++;
          validIdx[validCnt]=Point(gx,gz);
          mapPnt[gx][gz].clear();
          mapPnt[gx][gz].push_back(cpnt.y);
        }
      }
    }
  validFin=0;
  for (int i=1;i<=validCnt;i++)
  {
    int o=validIdx[i].x,p=validIdx[i].y;
    if (mapPnt[o][p].size()<gloconf.cl_ppg_th)
    {
      mapValid[o][p]=false;
      continue;
    }
    double cE;
    if (_Pc2DEM(mapPnt[o][p],cE)==0)
    {
      validFin++;
      mapDEM[validFin]=cE;
      mapX[validFin]=o-OIDX+xOffset;
      mapZ[validFin]=p-OIDX+zOffset;
    }
    else
    {
      mapValid[o][p]=false;
    }
  }
  if (validFin<gloconf.cl_vldfin_th)
    return -1;
  return 0;
}
