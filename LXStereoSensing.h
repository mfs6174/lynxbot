#ifndef LXSTEREOSENSING_H
#define LXSTEREOSENSING_H

//#define DISP_OUTPUT

struct LxStereoSensor
{
  Mat disp;
#ifdef DISP_OUTPUT
  Mat disp255;
#endif
  LxStereoSensor(Size ss);
  int LxSSProcess(Mat I1,Mat I2);
private:
  int32_t width;
  int32_t height;
  int32_t dims[3]; // bytes per line = width
  float* D1_data;
  float* D2_data;
  
};

#endif
