#ifndef LXLANDMARK_H
#define LXLANDMARK_H

#define FEAT_TYPE_SIFT
//#define FEAT_TYPE_SURF
#define FEAT_DIM 128 //特征描述子向量维数
#define NEED_DRAW //注释掉这个开关来加速如果不需要画出结果

#include "LXKDTree.h"
struct LxFeature
{
#ifdef FEAT_TYPE_SIFT
  SiftFeatureDetector detector;
  SiftDescriptorExtractor extractor;
#endif
#ifdef FEAT_TYPE_SURF
  SurfFeatureDetector detector;
  SurfDescriptorExtractor extractor;
#endif 
  void getFeature(Mat &img,vector<KeyPoint> &feat,Mat &des);
  void init();
};
//对图像局部特征的封装
//目前使用OpenCV SIFT,后续考虑采用GPU SIFT或OpenCV GPU::Surf

struct LxFOF
{
public:
  LxFeature lfeat;
  vector<KeyPoint> feat[2][2];
  Mat des[2][2];
  int kmap[2][10000][2];
  int regMap[5000][2];
  Point3d kp3d[2][10000];
  int idx;
  int num[2];
  int regNum;
  vector<Point3d> p3d1,p3d2;
  vector<Point2d> p2dl,p2dr;
  void init(LxStereoCam &_cam,Mat &img1,Mat &img2);
  int update(Mat &img1,Mat &img2,int uflag);
#ifdef NEED_DRAW
  void draw(Mat &pre,Mat &post,Mat &btw);
#endif
private:
  void makeKdByIndex();
  void updateByIndex(int ot,Mat &img1,Mat &img2);
  LxKDTree monoT;
  LxKDTree featT;
  bool unq[10000];
  LxStereoCam *cam;
#ifdef NEED_DRAW
  Mat monoimg[2],leftimg[2];
#endif
};
//帧特征管理管理类
//feat[idx][左-右] 保存特征点
//des[idx][左-右] 保存特征描述符
//kmap[idx][i][左-右] 左右视野中成功匹配的第i个特征点的编号
//regMap[i][前-后] 前后两帧(左视野)成功匹配(registered)的第i个特征点的编号
//kp3d[idx][i] 第i个左右匹配特征点的3D坐标
//idx 0/1滚动索引
//num[idx] 左右匹配特征点计数器
//regNum 前后匹配特征点计数
//p*d* 给VO的输出
//init 第一帧初始化
//update 逐帧更新
//makeKDbyIndex 对当前idx的左右成功匹配点建立KDtree(featT)等待搜索
//updateByIndex 对当前idx的左右视野特征点进行匹配
//unq 唯一性匹配标志

//注意这里存在一个问题 当匹配失败(0个点时)会出现段错误,需要处理
#endif
