#ifndef LXCALCULATE_H
#define LXCALCULATE_H

typedef void (*LMJac_P)( const Mat &src, Mat &dst ,int fLength);
typedef void (*LMFunc_P)( const Mat &src, Mat &dst ,int fLength);

double LxLevenbergMarquardt(LMJac_P jacobian,LMFunc_P function,const Mat &X0,Mat &resultX,
                            int maxIter,double eps,double tao,int fLength);

#endif
