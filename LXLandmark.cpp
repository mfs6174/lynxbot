#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXService.h"
#include "LXKDTree.h"
#include "LXCamera.h"
#include "LXLandmark.h"

extern LxConf gloconf;
extern LxKDTree *LXKDTree_Operating;

int lstp[20000];//用于保存KDtTree搜索结果的静态缓冲区

void LxFeature::getFeature(Mat &img,vector<KeyPoint> &feat,Mat &des)
{
  detector.detect(img,feat);
  extractor.compute(img,feat,des);
}

void LxFeature::init()
{
}

void LxFOF::updateByIndex(int ot,Mat &img1,Mat &img2)
{
#ifdef NEED_DRAW
  Mat gray,temp;
  cvtColor(img1,gray,CV_RGB2GRAY);
  gray.copyTo(leftimg[idx]);
  monoimg[idx].create(img1.size().height,img1.size().width*2,CV_8UC3);
  temp=monoimg[idx].colRange(0,img1.size().width);
  cvtColor(gray,temp,CV_GRAY2RGB);
  cvtColor(img2,gray,CV_RGB2GRAY);
  temp=monoimg[idx].colRange(img1.size().width,img1.size().width*2);
  cvtColor(gray,temp,CV_GRAY2RGB);
#endif
  num[idx]=0;//清空计数器
  lfeat.getFeature(img1,feat[idx][0],des[idx][0]);
  lfeat.getFeature(img2,feat[idx][1],des[idx][1]);
  monoT.set(2,feat[idx][1].size());
  monoT.nodes[1].rect[1][0]=monoT.nodes[1].rect[0][0]=10000;
  monoT.nodes[1].rect[0][1]=monoT.nodes[1].rect[1][1]=-1;
  for (int i=0;i<feat[idx][1].size();i++)
  {
    monoT.data[i+1].d[0]=feat[idx][1][i].pt.x;
    monoT.data[i+1].d[1]=feat[idx][1][i].pt.y;
    monoT.data[i+1].dim=2;
    if (feat[idx][1][i].pt.x<monoT.nodes[1].rect[0][0])
      monoT.nodes[1].rect[0][0]=feat[idx][1][i].pt.x;
    if (feat[idx][1][i].pt.x>monoT.nodes[1].rect[0][1])
      monoT.nodes[1].rect[0][1]=feat[idx][1][i].pt.x;
    if (feat[idx][1][i].pt.y<monoT.nodes[1].rect[1][0])
      monoT.nodes[1].rect[1][0]=feat[idx][1][i].pt.y;
    if (feat[idx][1][i].pt.y>monoT.nodes[1].rect[1][1])
      monoT.nodes[1].rect[1][1]=feat[idx][1][i].pt.y;
    monoT.pset[i+1]=i+1;
  }//将数据存入monoT,设置根节点范围为所有特征点的包围矩形
  LXKDTree_Operating=&monoT;//设置当前操作的KDTree指针
  monoT.build(1,1,feat[idx][1].size(),0);//建树
  //以下搜索过程参考武二永那篇论文
  memset(unq,0,sizeof(unq));
  float _r[2][2];
  for (int i=0;i<feat[idx][0].size();i++)
  {
    _r[0][0]=feat[idx][0][i].pt.x-gloconf.ldmk_dispa_max;
    _r[0][1]=feat[idx][0][i].pt.x-gloconf.ldmk_dispa_min;
    _r[1][0]=feat[idx][0][i].pt.y-gloconf.ldmk_el_error;
    _r[1][1]=feat[idx][0][i].pt.y+gloconf.ldmk_el_error;
    monoT.rangeSearch(1,_r,lstp);
    //设置搜索范围并做Range_search
    if (lstp[0]>1)//如果range内有1个以上的点
    {
      float mm=1e200,cmm=1e200,t;
      int mid;
      for (int j=1;j<=lstp[0];j++)
      {
        t=Mat(( des[idx][0].row(i)-des[idx][1].row(lstp[j]-1) )*(des[idx][0].row(i)-des[idx][1].row(lstp[j]-1)).t()).at<float>(0);
        if (t<mm)
        {
          mm=t;
          mid=lstp[j]-1;
        }
        else
          if (t<cmm)
          {
            cmm=t;
          }
      }//找最近次近点
      if ( (!unq[mid]) && (abs(feat[idx][0][i].angle-feat[idx][1][mid].angle)<gloconf.ldmk_ori_error) && (mm<=gloconf.ldmk_des_dist_th1||(mm<=gloconf.ldmk_des_dist_th2&&mm/cmm<gloconf.ldmk_des_rat_th1)||(mm/cmm<gloconf.ldmk_des_rat_th2)) )
      {
        unq[mid]=true;
        num[idx]++;
        kmap[idx][num[idx]][0]=i;
        kmap[idx][num[idx]][1]=mid;
      }//满足匹配条件则匹配(多级条件)
    }
    else
    {
      if (lstp[0]<1)
        continue;
      if ( (!unq[lstp[1]-1]) && (abs(feat[idx][0][i].angle-feat[idx][1][lstp[1]-1].angle)<gloconf.ldmk_ori_error) )
      {
        unq[lstp[1]-1]=true;
        num[idx]++;
        kmap[idx][num[idx]][0]=i;
        kmap[idx][num[idx]][1]=lstp[1]-1;
      }
    }//如果只有1个点只检查主梯度方向和唯一性约束
  }
  for (int i=1;i<=num[idx];i++)
    kp3d[idx][i]=cam->reproject(Point2d(feat[idx][0][kmap[idx][i][0]].pt.x,feat[idx][0][kmap[idx][i][0]].pt.y),-feat[idx][1][kmap[idx][i][1]].pt.x+feat[idx][0][kmap[idx][i][0]].pt.x);
  //做3D反投影
}
  
void LxFOF::makeKdByIndex()//左右匹配点以左视野描述子存入featT并建树
{
  featT.set(FEAT_DIM,num[idx]);
  LXKDTree_Operating=&featT;
  for (int i=1;i<=num[idx];i++)
  {
    featT.data[i].dim=FEAT_DIM;
    for (int j=0;j<FEAT_DIM;j++)
      featT.data[i].d[j]=des[idx][0].at<float>(kmap[idx][i][0],j);
    featT.pset[i]=i;
  }
  featT.build(1,1,num[idx],0);
}

void LxFOF::init(LxStereoCam &_cam,Mat &img1,Mat &img2)
{
  cam=&_cam;
  num[0]=num[1]=regNum=idx=0;
  monoT.set(2,3000);
  featT.set(FEAT_DIM,2000);
  lfeat.init();
  updateByIndex(1,img1,img2);
  makeKdByIndex();    
}

#ifdef NEED_DRAW
void LxFOF::draw(Mat &pre,Mat &post,Mat &btw)
{
  Mat temp;
  btw.create(leftimg[0].size().height,leftimg[0].size().width*2,CV_8UC3);
  temp=btw.colRange(0,leftimg[0].size().width);
  cvtColor(leftimg[idx],temp,CV_GRAY2RGB);
  temp=btw.colRange(leftimg[0].size().width,leftimg[0].size().width*2);
  cvtColor(leftimg[1-idx],temp,CV_GRAY2RGB);
  for (int i=1;i<=num[idx];i++)
    line(monoimg[idx],Point(feat[idx][0][kmap[idx][i][0]].pt.x,feat[idx][0][kmap[idx][i][0]].pt.y),Point(feat[idx][1][kmap[idx][i][1]].pt.x+leftimg[0].size().width,feat[idx][1][kmap[idx][i][1]].pt.y),CV_RGB(LxRandServ::getn(255),LxRandServ::getn(255),LxRandServ::getn(255)));
  for (int i=1;i<=num[1-idx];i++)
    line(monoimg[1-idx],Point(feat[1-idx][0][kmap[1-idx][i][0]].pt.x,feat[1-idx][0][kmap[1-idx][i][0]].pt.y),Point(feat[1-idx][1][kmap[1-idx][i][1]].pt.x+leftimg[0].size().width,feat[1-idx][1][kmap[1-idx][i][1]].pt.y),CV_RGB(LxRandServ::getn(255),LxRandServ::getn(255),LxRandServ::getn(255)));
  for (int i=1;i<=regNum;i++)
    line(btw,Point(feat[idx][0][regMap[i][1]].pt.x,feat[idx][0][regMap[i][1]].pt.y),Point(feat[1-idx][0][regMap[i][0]].pt.x+leftimg[0].size().width,feat[1-idx][0][regMap[i][0]].pt.y),CV_RGB(LxRandServ::getn(255),LxRandServ::getn(255),LxRandServ::getn(255)));
  pre=monoimg[1-idx];
  post=monoimg[idx];
}
#endif

int LxFOF::update(Mat &img1,Mat &img2,int uflag)
{
  int ot;
  if (uflag==0)
  {
    ot=idx;
    idx=1-idx;
  }
  else
  {
    ot=1-idx;
  }
  if (uflag<2)
  {
    updateByIndex(ot,img1,img2);
    //更新滚动索引,初始化
    regNum=0;
    memset(unq,0,sizeof(unq));
    p3d1.clear();p3d2.clear();p2dl.clear();p2dr.clear();
    for (int i=1;i<=num[idx];i++)
    {
      float tar[FEAT_DIM];
      for (int j=0;j<FEAT_DIM;j++)
        tar[j]=des[idx][0].at<float>(kmap[idx][i][0],j);
      featT.aknns(tar,lstp,2,gloconf.kd_search_sigma);
      float tmp1=featT.data[lstp[1]].dist(tar),tmp2=featT.data[lstp[2]].dist(tar);
      if ( (!unq[lstp[1]])&&((tmp1<gloconf.ldmk_des_dist_th1)||(tmp1<=gloconf.ldmk_des_dist_th2&&tmp1/tmp2<gloconf.ldmk_des_rat_th1)||(tmp1/tmp2<gloconf.ldmk_des_rat_th3)) )
      {
        unq[lstp[1]]=true;
        regNum++;
        regMap[regNum][1]=kmap[idx][i][0];
        regMap[regNum][0]=kmap[ot][lstp[1]][0];
        p3d1.push_back(kp3d[idx][i]);
        p3d2.push_back(kp3d[ot][lstp[1]]);
        p2dl.push_back(Point2d(feat[ot][0][kmap[ot][lstp[1]][0]].pt.x,feat[ot][0][kmap[ot][lstp[1]][0]].pt.y));
        p2dr.push_back(Point2d(feat[ot][1][kmap[ot][lstp[1]][1]].pt.x,feat[ot][1][kmap[ot][lstp[1]][1]].pt.y));
      }
    }//在上一帧特征的KDTree中搜索本帧特征的对应点,如果成功则记录编号,生成2D,2D点的vector
    return p3d1.size();
  }
  else
    makeKdByIndex();
  //对本帧特征建KDTree
  return 0;
}
