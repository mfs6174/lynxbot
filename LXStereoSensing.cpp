#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXService.h"

#include "LynXTplib/libelas/elas.h"
#include "LynXTplib/libelas/image.h"

#include "LXStereoSensing.h"


extern LxConf gloconf;

LxStereoSensor::LxStereoSensor(Size framsize)
{
  width=framsize.width;
  height=framsize.height;
  dims[0]=width;dims[1]=height;dims[2]=width;
  //disp.create(height,width,CV_32FC1);
  D1_data=(float*)malloc(width*height*sizeof(float));
  D2_data=(float*)malloc(width*height*sizeof(float));
#ifdef DISP_OUTPUT
  disp255.create(height,width,CV_8U);
#endif
}

int LxStereoSensor::LxSSProcess(Mat I1,Mat I2)
{
  CV_Assert(I1.rows==height&&I1.cols==width&&I2.rows==height&&I2.cols==width);
  // process
  Elas::setting inits=(Elas::setting)2;
  Elas::parameters param(inits);
  param.postprocess_only_left = true;
  Elas elas(param);
  // cout<<sizeof(I1.data)<<' '<<sizeof(I2.data)<<endl;
  if ( elas.process(I1.data,I2.data,D1_data,D2_data,dims) )
    return -1;
  disp=Mat(height,width,CV_32FC1,D1_data);
#ifdef DISP_OUTPUT
  ofstream fout("NR.disp.dat");
  float disp_max = 0,disp_min=5000;
  for (int32_t i=0; i<width*height; i++)
  {
    if (D1_data[i]>disp_max) disp_max = D1_data[i];
    if (D1_data[i]!=-10&&D1_data[i]<disp_min) disp_min = D1_data[i];
  }
  for (int i=0;i<height;i++)
  {
    for (int j=0;j<width;j++)
    {
      disp255.at<uchar>(i,j)=(uchar)max(255.0*disp.at<float>(i,j)/(disp_max),0.0);
      fout<<disp.at<float>(i,j)<<' ';
    }
    fout<<endl;
  }
  image<uchar> *D1 = new image<uchar>(width,height);
  for (int32_t i=0; i<width*height; i++)
    D1->data[i] = (uint8_t)max(255.0*D1_data[i]/(disp_max),0.0);
  cout<<disp_min<<endl;
  cout<<disp_max-disp_min<<endl;
  savePGM(D1,"disp.pgm");
  delete D1;
#endif
  return 0;
}

