#include "LXCommonHeader.h"

#include "LXService.h"

LxDtimer::LxDtimer()
{
  stick = (double)getTickCount();
  ntick = stick;
}

double LxDtimer::dt()
{
  double t = (double)getTickCount();
  double ddt = (t-ntick)/getTickFrequency();
  ntick=t;
  return ddt*1000;
}

double LxDtimer::st()
{
  double t=(double)getTickCount();
  return (t-stick)/getTickFrequency()*1000;
}

void LxDtimer::reset()
{
  stick = (double)getTickCount();
  ntick = stick;
}

LxTimeServ::LxTimeServ()
{
  ttime=time(NULL);
}

void LxTimeServ::reset()
{
  ttime=time(NULL);
};

void LxTimeServ::start()
{
  stime=time(NULL);
}

llg LxTimeServ::uptime()
{
  time_t ntime=time(NULL);
  return ntime-stime;
}

llg LxTimeServ::dt()
{
  time_t ntime=time(NULL);
  return ntime-ttime;
} 

void LxRandServ::start()
{
  srand(time(NULL));
}

double LxRandServ::get01()
{
  return (double)(rand()%100000000/100000000);
}

llg LxRandServ::getn(llg n)
{
  return rand()%n;
}
