#ifndef LXCAMERA_H
#define LXCAMERA_H

struct LxCam
{
public:
  Mat raw,frame,nframe;
  Size fsize;
  int id;
  
  LxCam(int idx);
  void init(string cmf,string disf);
  void cgrab();
  void cresize();
  void cremap();
  void refresh(Mat &m);
  void setpro();
private:
  VideoCapture cap;
  bool ifud;
  Mat rmapx,rmapy;
  Mat cmo,dist,cmn;
};

struct LxStereoCam
{
public:
  Mat raw1,raw2,frame1,frame2,nframe1,nframe2;
  Mat _p1,_p2;
  double f,cx1,cx2,cy,tf,t1,dt;
  Size fsize;
  int id1,id2;
  LxStereoCam(int idx1,int idx2);
  void init(const string &camf1,const string &camf2,const string &rf);
  void cgrab();
  void cresize();
  void cremap();
  void refresh(Mat &m1,Mat &m2);
  void setpro();
  Point2d project(Point3d p,int LR) const;
  Point3d reproject(Point2d p,double dis) const;
  void reproject(const Mat &dis,Mat &_3dmap) const;
private:
  VideoCapture cap1,cap2;
  bool ifr;
  Mat cmo1,cmo2,dist1,dist2,_q;
  Mat rmapx1,rmapx2,rmapy1,rmapy2;
};

#endif
