#ifndef LXMISSION_H
#define LXMISSION_H

struct LxMission
{
  float botW,botL,botH;
  LxGMTrans botPos,camPos;
  Point3d botLoc;
  double roll,pitch,heading;
  int xLb,xUb,zLb,zUb;
  Point destP,lastDest;
  int gpTimes,mvTimes,loopCount;
  int maxWorkingTime;
  bool rgpFlag,rgdFlag,mvFlag,trFlag,sysFailFlag,homingFlag,voState;
  int pathIdx;
  vector<Point> curPath;
  vector<Point> traceRec;
  
  LxStereoCam *scam;
  LxFOF fof;
  LxMap *mmap;
  LxStereoSensor *stes;
  LxPointCLoud *ppcl;
  LxController ctl;
  Mat framebuf1,framebuf2,drawbuf1,drawbuf2,drawbuf3,gframe1,gframe2;
  LxGMTrans curTrans;
  
  int missionInit(const LxGMTrans &_cpos,int _xl,int _xu,int _zl,int _zu,int _mwt);
  int mainLoop(int maxLoop);
  void parsePos();
  void doTurn(bool flag);
  ~LxMission();
};

#define TURNTIME 300
#define MOVETIME 330

#define MANC
#define DEBUGP
#endif
