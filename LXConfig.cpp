#include "LXCommonHeader.h"
#include "LXConfig.h"

extern LxConf gloconf;

LxConf::LxConf()
{
  cam_dev_id[0]=0;
  cam_dev_id[1]=1;
  
  framesize=Size(800,600);
  exposure=5000;
  autoexposure=false;

  kd_search_sigma=0.5;

  ldmk_el_error=5;
  ldmk_dispa_min=8,ldmk_dispa_max=280;
  ldmk_ori_error=22.5;
  ldmk_des_dist_th1=10000,ldmk_des_dist_th2=40000;
  ldmk_des_rat_th1=0.36,ldmk_des_rat_th2=0.25,ldmk_des_rat_th3=0.17;

  cl_ppg_th=100;
  cl_vldfin_th=5;
  cl_lyr_len=50.0;
  cl_pnpl_hth=10;

  bot_max_cob=50.0;
  
  map_upd_coe1=0.85;
  map_upd_coe2=0.6;

  bot_width=250;
  bot_length=500;
  bot_height=200;
  bot_cam_h=200;

  ms_max_gpt=3;
  ms_maxlc_bfrgd=30;

  vo_proj_maxerr=25.0;
}

bool LxConf::load(string cf)
{
  FileStorage fs(cf, FileStorage::READ);
  if (!fs.isOpened())
    return false;

  cam_dev_id[0]=(int)fs["cam_dev_id0"];
  cam_dev_id[1]=(int)fs["cam_dev_id1"];
  framesize=Size((int)fs["framesizew"],(int)fs["framesizeh"]);
  exposure=(int)fs["exposure"];
  autoexposure=(int)fs["autoexposure"];

  kd_search_sigma=(float)fs["kd_search_sigma"];

  ldmk_el_error=(float)fs["ldmk_el_error"];
  ldmk_dispa_min=(float)fs["ldmk_dispa_min"];
  ldmk_dispa_max=(float)fs["ldmk_dispa_max"];
  ldmk_ori_error=(float)fs["ldmk_ori_error"];
  ldmk_des_dist_th1=(float)fs["ldmk_des_dist_th1"];
  ldmk_des_dist_th2=(float)fs["ldmk_des_dist_th2"];
  ldmk_des_rat_th1=(float)fs["ldmk_des_rat_th1"];
  ldmk_des_rat_th2=(float)fs["ldmk_des_rat_th2"];
  ldmk_des_rat_th3=(float)fs["ldmk_des_rat_th3"];

  cl_ppg_th=(int)fs["cl_ppg_th"];
  cl_vldfin_th=(int)fs["cl_vldfin_th"];
  cl_lyr_len=(float)fs["cl_lyr_len"];
  cl_pnpl_hth=(int)fs["cl_pnpl_hth"];

  bot_max_cob=(float)fs["bot_max_cob"];
  
  map_upd_coe1=(float)fs["map_upd_coe1"];
  map_upd_coe2=(float)fs["map_upd_coe2"];

  bot_width=(float)fs["bot_width"];
  bot_length=(float)fs["bot_length"];
  bot_height=(float)fs["bot_height"];
  bot_cam_h=(float)fs["bot_cam_h"];

  ms_max_gpt=(int)fs["ms_max_gpt"];
  ms_maxlc_bfrgd=(int)fs["ms_maxlc_bfrgd"];

  vo_proj_maxerr=(float)fs["vo_proj_maxerr"];
  fs.release();
  return true;
}

bool LxConf::save(string cf)
{
  FileStorage fs(cf, FileStorage::WRITE);
  if (!fs.isOpened())
    return false;
  fs<<"cam_dev_id0"<<cam_dev_id[0];
  fs<<"cam_dev_id1"<<cam_dev_id[1];
  
  fs<<"framesizew"<<framesize.width<<"framesizeh"<<framesize.height;
  fs<<"exposure"<<exposure;
  fs<<"autoexposure"<<autoexposure;

  fs<<"kd_search_sigma"<<kd_search_sigma;

  fs<<"ldmk_el_error"<<ldmk_el_error;
  fs<<"ldmk_dispa_min"<<ldmk_dispa_min;
  fs<<"ldmk_dispa_max"<<ldmk_dispa_max;
  fs<<"ldmk_ori_error"<<ldmk_ori_error;
  fs<<"ldmk_des_dist_th1"<<ldmk_des_dist_th1;
  fs<<"ldmk_des_dist_th2"<<ldmk_des_dist_th2;
  fs<<"ldmk_des_rat_th1"<<ldmk_des_rat_th1;
  fs<<"ldmk_des_rat_th2"<<ldmk_des_rat_th2;
  fs<<"ldmk_des_rat_th3"<<ldmk_des_rat_th3;

  fs<<"cl_ppg_th"<<cl_ppg_th;
  fs<<"cl_vldfin_th"<<cl_vldfin_th;
  fs<<"cl_lyr_len"<<cl_lyr_len;
  fs<<"cl_pnpl_hth"<<cl_pnpl_hth;

  fs<<"bot_max_cob"<<bot_max_cob;
  
  fs<<"map_upd_coe1"<<map_upd_coe1;
  fs<<"map_upd_coe2"<<map_upd_coe2;

  fs<<"bot_width"<<bot_width;
  fs<<"bot_length"<<bot_length;
  fs<<"bot_height"<<bot_height;
  fs<<"bot_cam_h"<<bot_cam_h;

  fs<<"ms_max_gpt"<<ms_max_gpt;
  fs<<"ms_maxlc_bfrgd"<<ms_maxlc_bfrgd;

  fs<<"vo_proj_maxerr"<<vo_proj_maxerr;
  fs.release();
  return true;
}


