#ifndef LXGEOMETRY_H
#define LXGEOMETRY_H

struct LxGMTrans
{
  Mat R,T;
  double qw,qx,qy,qz;
  LxGMTrans();
  void setR(const Mat &iR);
  void setR(double y,double p,double r);
  void setT(const Mat &it);
  void setT(const Point3d &p);
  Point3d trans(const Point3d &ip) const;
  void acc(const LxGMTrans &t);
  void getER(double &y,double &r,double &p) const;
  void ass(const LxGMTrans &t);
  void setRfromQuater(const Mat &quad);
  void getParaVec(Mat &m) const;
  Point3d getLoc() const;
};


#endif
