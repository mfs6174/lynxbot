#include "LXCommonHeader.h"
#include "LXCalculate.h"

double LxLevenbergMarquardt(LMJac_P jacobian,LMFunc_P function,const Mat &X0,Mat &resultX,
                            int maxIter,double eps,double tao,int fLength)
{
  /* This is not sparce method */
  /* Make optimization using  */
  /* func - function to compute */
  /* uses function to compute jacobian */
  
  /* ========== Initlization ============ */

  int numVal=X0.size().height;
  int numFunc=fLength*4;
  double valError;
  double valNewError;
  Mat errorMat(1,1,CV_64F);
  Mat X(numVal,1,CV_64F),newX(numVal,1,CV_64F),resF(numFunc,1,CV_64F),newResF(numFunc,1,CV_64F),H(numVal,numVal,CV_64F);
  Mat J(numFunc,numVal,CV_64F),Hlm(numVal,1,CV_64F),g(numVal,1,CV_64F),newH(numVal,numVal,CV_64F);
  jacobian(X0,J,fLength);
  H=J.t()*J;
  double pmax=-1e20;
  for (int i=0;i<numVal;i++)
    if (H.at<double>(i,i)>pmax)
      pmax=H.at<double>(i,i);
  double change=1;
  int currIter=0;
  double alpha=tao*pmax;
  bool ifUpdate=true;
  X0.copyTo(X);
  /* Compute value of f */
  function(X,resF,fLength);
  /* Need to use new version of computing error (norm) */
  valError=(errorMat=resF.t()*resF).at<double>(0);
  /* ========== Main optimization loop ============ */
  do {
    currIter++;
    if (ifUpdate)
    {
      /* Compute Jacobian for given point vectX */
      jacobian(X,J,fLength);
      /* Define optimal delta for J'*J*delta=J'*error */
      /* compute J'J */
      H=J.t()*J;
      /* compute J'*error */
      g=J.t()*resF;
    }        
    /* Increase diagonal elements by alpha */
    newH=H+alpha*Mat::eye(numVal,numVal,CV_64F);
    /* Solve system to define delta */
    solve(newH,g,Hlm,CV_SVD);
    /* We know delta and we can define new value of vector X */
    newX=X+Hlm;
    /* Compute result of function for new vector X */
    function(newX,newResF,fLength);
    valNewError = (errorMat=newResF.t()*newResF).at<double>(0);

    if( valNewError < valError )
    {/* accept new value */
      valError = valNewError;
      newResF.copyTo(resF);
      /* Compute relative change of required parameter vectorX. change = norm(curr-prev) / norm(curr) )  */
      change = norm(X,newX, CV_RELATIVE_L2);
      alpha /= 10;
      newX.copyTo(X);
      ifUpdate=true;
    }
    else
    {
      ifUpdate=false;
      alpha *= 10;
    }
  }while ( change > eps && currIter < maxIter );
  
/* result was computed */
  X.copyTo(resultX);
  return valError;
}
