#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXCamera.h"
#include "LXService.h"
#include "LXKDTree.h"
#include "LXLandmark.h"
#include "LXGeometry.h"
#include "LXVisualOdometry.h"
#include "LXStereoSensing.h"

LxConf gloconf;
time_t LxTimeServ::stime=0;

int main()
{
  LxTimeServ::start();
  LxRandServ::start();
  gloconf.load("LynXData/settings.xml");
  // LxCam cam0(1);
  //cam0.init("LynXData/CamMat1.xml","");
  LxStereoCam scam(gloconf.cam_dev_id[0],gloconf.cam_dev_id[1]);
  scam.init("LynXData/CamMat1.xml","LynXData/CamMat2.xml","LynXData/MonoData.xml");
  namedWindow("video1",1);
  namedWindow("video2",1);
  namedWindow("btw",1);
  namedWindow("disparity",1);
  LxDtimer timer1;
  Mat frame1,frame2,gframe1,gframe2;
  scam.refresh(frame1,frame2);
  LxFOF fof;
  fof.init(scam,frame1,frame2);
  LxGMTrans state,upd;
  LxStereoSensor stes(gloconf.framesize);
  int cnt=0;
  for (;;)
  {
    // if (timer1.st()>5000)
    // {
    //   scam.setpro();
    //   timer1.reset();
    // }
    scam.refresh(frame1,frame2);
    imshow("video1",frame1);
    imshow("video2",frame2);
    cvtColor(frame1,gframe1,CV_RGB2GRAY);
    cvtColor(frame2,gframe2,CV_RGB2GRAY);
    if (stes.LxSSProcess(gframe1,gframe2))
      cout<<"fail"<<endl;
    imshow("disparity",stes.disp255);
    int wk;
    Mat si1,si2,si3;
    if ((wk=waitKey(100))==KEY_Z)
    {
      fof.update(frame1,frame2);
      fof.draw(si1,si2,si3);
      imshow("video1",si1);
      imshow("video2",si2);
      imshow("btw",si3);
      cnt++;
      stringstream id;
      id<<cnt;
      imwrite("frame1"+id.str()+".pgm",gframe1);
      imwrite("frame2"+id.str()+".pgm",gframe2);
      if (waitKey(0)==KEY_Z)
      {
        // Mat tmp;
        // resize(frame1,tmp,Size(400,300));
        // resize(frame2,tmp,Size(400,300));
        // imwrite("si1"+id.str()+".png",si1);
        // imwrite("si2"+id.str()+".png",si2);
        // imwrite("si3"+id.str()+".png",si3);
        LxVOUpdate(fof.p3d1,fof.p3d2,upd,fof.p2dl,fof.p2dr,scam);
        state.acc(upd);
        double yaw,roll,pitch;
        upd.getER(yaw,roll,pitch);
        printf("T: %.2lf %.2lf %.2lf  R: %.3lf %.3lf %.3lf\n\n",
               upd.T.at<double>(0),upd.T.at<double>(1),upd.T.at<double>(2),yaw,roll,pitch);
        state.getER(yaw,roll,pitch);
        printf("T: %.2lf %.2lf %.2lf  R: %.3lf %.3lf %.3lf\n\n",
               state.T.at<double>(0),state.T.at<double>(1),state.T.at<double>(2),yaw,roll,pitch);
      }
    }
    else
    {
      if (wk>0)
        break;
    }
  }
  destroyWindow("video1");
  destroyWindow("video2");
  gloconf.save("LynXData/settings.xml");
  cout<<"We running for "<<(LxTimeServ::uptime())<<" seconds"<<endl;
  return 0;
}
