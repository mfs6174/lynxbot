#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXService.h"
#include "LXMap.h"

extern LxConf gloconf;

LxMBlock::LxMBlock()
{
  h=0,deg=0;
  obvStatus=0;
  expStatus=false;
  obvTimes=0;
  passAble=false;
  cannotPass=false;
  cdX=0,cdZ=0;
}

LxMap::LxMap(int xl,int xu,int zl,int zu,double gsize)
{
  mxOff=MXOFF;mzOff=MZOFF;
  xLb=xl;
  xUb=xu;
  zLb=zl;
  zUb=zu;
  gridSize=gsize;
  memset(mapStore,0,sizeof(mapStore));
  otm=Mat::zeros(ZDIM*10+10, XDIM*10+10, CV_8UC3);
}

int LxMap::_getCoord(double x)
{
  double t=x/gridSize;
  int i=(int)t;
  double f=t-i;
  int p;
  if (abs(f)<=0.5)
    p=0;
  else
    p=1;
  if (t<0)
    p=-p;
  return i+p;
}

inline LxMBlock* LxMap::gIdx(int gx,int gz)
{
  if (gx<xLb||gx>xUb||gz<zLb||gz>zUb)
    return NULL;
  return &mapStore[gx+mxOff][gz+mzOff];
}

inline LxMBlock* LxMap::gLoc(double x,double z)
{
  int gx=_getCoord(x),gz=_getCoord(z);
  return gIdx(gx,gz);
 }

inline int LxMap::_setBoolIdx(int gx,int gz)
{
  if (gx<xLb||gx>xUb||gz<zLb||gz>zUb)
    return -1;
  if (!boolMap[gx+mxOff][gz+mzOff])
  {
    boolMap[gx+mxOff][gz+mzOff]=1;
    return 0;
  }
  return 1;
}

inline int LxMap::_setBoolLoc(double x,double z)
{
  int gx=_getCoord(x),gz=_getCoord(z);
  return _setBoolIdx(gx,gz);
}

inline int LxMap::_setCharIdx(int gx,int gz,uchar para)
{
  if (gx<xLb||gx>xUb||gz<zLb||gz>zUb)
    return -1;
  if (para==255)
    return boolMap[gx+mxOff][gz+mzOff];
  uchar tmp=boolMap[gx+mxOff][gz+mzOff];
  boolMap[gx+mxOff][gz+mzOff]=para;
  return tmp;
}

inline int LxMap::_setCharLoc(double x,double z,uchar para)
{
  int gx=_getCoord(x),gz=_getCoord(z);
  return _setCharIdx(gx,gz,para);
}

void LxMap::update(int bcnt,int mapX[],int mapZ[],double mapDem[],double lat)
{
  for (int i=1;i<=bcnt;i++)
  {
    LxMBlock *wp=gIdx(mapX[i],mapZ[i]);
    if (wp==NULL)
      continue;
    if (wp->obvStatus==2)
      continue;
    if (wp->obvStatus==0)
    {
      wp->cdX=mapX[i];
      wp->cdZ=mapZ[i];
      wp->obvStatus=1;
      wp->obvTimes=1;
      wp->h=mapDem[i];
      if (abs(lat-wp->h)<=gloconf.bot_max_cob)
        wp->passAble=true;
      else
        wp->passAble=false;
      continue;
    }
    if (wp->obvStatus==1)
    {
      wp->obvTimes++;
      if ( abs(lat-mapDem[i])<=gloconf.bot_max_cob && gIdx(mapX[i],mapZ[i])->passAble==false)
        wp->h=(wp->h)*gloconf.map_upd_coe1+(1-gloconf.map_upd_coe1)*mapDem[i];
      else
      {
        if ( abs(lat-mapDem[i])>gloconf.bot_max_cob && gIdx(mapX[i],mapZ[i])->passAble==true)
          wp->h=(wp->h)*(1-gloconf.map_upd_coe1)+gloconf.map_upd_coe1*mapDem[i];
        else
          wp->h=(wp->h)*gloconf.map_upd_coe2+(1-gloconf.map_upd_coe2)*mapDem[i];
      }
      if (abs(lat-wp->h)<=gloconf.bot_max_cob)
        wp->passAble=true;
      else
        gIdx(mapX[i],mapZ[i])->passAble=false;
    }
  } 
}

void LxMap::updateByBot(const Point3d &sp)
{
  int gx=_getCoord(sp.x),gz=_getCoord(sp.z);
  if (gIdx(gx,gz)!=NULL)
  {
    if (gIdx(gx,gz)->obvStatus==0)
    {
      gIdx(gx,gz)->cdX=gx;
      gIdx(gx,gz)->cdZ=gz;
    }
    gIdx(gx,gz)->obvStatus=2;
    gIdx(gx,gz)->passAble=true;
    gIdx(gx,gz)->expStatus=true;
    gIdx(gx,gz)->h=sp.y;
  }
}

int LxMap::getNearest(double bx,double bz,Point &np)
{
  LxMBlock *wp=NULL;
  LxMBlock *pp=NULL;
  memset(boolMap,0,sizeof(boolMap));
  tQ=queue<LxMBlock *>();
  tQ.push(gLoc(bx,bz));
  _setBoolLoc(bx,bz);
  while (!tQ.empty())
  {
    wp=tQ.front();
    tQ.pop();
    for (int i=-1;i<=1;i++)
      for (int j=-1;j<=1;j++)
      {
        if (_setBoolIdx(wp->cdX+i,wp->cdZ+j))
          continue;
        pp=gIdx(wp->cdX+i,wp->cdZ+j);
        if (pp->obvStatus<1&&pp->expStatus==false&&(!pp->cannotPass))
        {
          pp->expStatus=true;
          pp->cdX=wp->cdX+i;
          pp->cdZ=wp->cdZ+j;
          np.x=pp->cdX;
          np.y=pp->cdZ;
          tQ=queue<LxMBlock *>();
          return 0;
        }
        else
        {
          tQ.push(pp);
        }
      }
  }
  tQ=queue<LxMBlock *>();
  return -1;
}

int LxMap::getPath(int dx,int dz,double bx,double bz,vector<Point> &path)
{
  memset(boolMap,0,sizeof(boolMap));
  openList=multiset<LxMAS>();
  path.clear();
  _setCharLoc(bx,bz,1);
  gLoc(bx,bz)->openP=openList.insert(LxMAS(0,0,gLoc(bx,bz)));
  gLoc(bx,bz)->ft=NULL;
  LxMAS ws;
  while (!openList.empty())
  {
    ws=*(openList.begin());
    openList.erase(openList.begin());
    int tx=ws.p->cdX,tz=ws.p->cdZ;
    _setCharIdx(tx,tz,2);
    for (int ii=-1;ii<=1;ii++)
      for (int jj=-1;jj<=1;jj++)
      {
        if (_setCharIdx(tx+ii,tz+jj,255)==-1||_setCharIdx(tx+ii,tz+jj,255)>1)
          continue;
        LxMBlock *tpt=gIdx(tx+ii,tz+jj);
        if (tpt->obvStatus==0)
          continue;
        if (abs(tpt->h-ws.p->h)>gloconf.bot_max_cob)
          continue;
        if (tpt->cannotPass)
          continue;
        int ng=ws.g;
        if (abs(ii)+abs(jj)>1)
          ng+=14;
        else
          ng+=10;
        if (_setCharIdx(tx+ii,tz+jj,1)==0)
        {
          if (tx+ii==dx&&tz+jj==dz)
          {
            path.push_back(Point(dx,dz));
            tpt=gIdx(tx,tz);
            while (tpt!=NULL)
            {
              path.push_back(Point(tpt->cdX,tpt->cdZ));
              tpt=tpt->ft;
            }
            reverse(path.begin(),path.end());
            return 0;
          }
          tpt->openP=openList.insert(LxMAS(ng,(abs(tx+ii-dx)+abs(tz+jj-dz))*10,tpt));
          tpt->ft=ws.p;
        }
        else
        {
          if (ng<((*(tpt->openP)).g))
          {
            int th=(*(tpt->openP)).f-(*(tpt->openP)).g;
            openList.erase(tpt->openP);
            tpt->openP=openList.insert(LxMAS(ng,th,tpt));
            tpt->ft=ws.p;
          }
        }
      }
  }
  return -1;
}

inline uchar LxMap::_getColor(double lat)
{
  static uchar cma=230,cmi=10;
  if (abs(lat)>=5000.0)
    return cmi;
  return (uchar)((5000-abs(lat))/5000.0*(cma-cmi)+cmi);
}

inline void LxMap::_setColor(int x,int y,uchar r,uchar g,uchar b)
{
  otm.at<Vec3b>(x,y)[0]=r;
  otm.at<Vec3b>(x,y)[1]=g;
  otm.at<Vec3b>(x,y)[2]=b;
}

int LxMap::outputMap(const string &fname,const Point3d &bloc,const vector<Point> &_path)
{
  for (int i=xLb;i<=xUb;i++)
    for (int j=zLb;j<=zUb;j++)
    {
      LxMBlock *wp=gIdx(i,j);
      int xx=(j+mzOff)*10,yy=(i+mxOff)*10;
      if (wp->obvStatus<1)
        _setColor(xx,yy,255,255,255);
      else
        _setColor(xx,yy,0,_getColor(wp->h),0);
      if (wp->cannotPass)
        _setColor(xx,yy,0,0,0);
      for (int ii=1;ii<=9;ii++)
        for (int jj=1;jj<=9;jj++)
          for (int cc=0;cc<3;cc++)
            otm.at<Vec3b>(xx+ii,yy+jj)[cc]=otm.at<Vec3b>(xx,yy)[cc];
    }
  for (int i=0;i<_path.size();i++)
  {
    int xx=(_path[i].y+mzOff)*10,yy=(_path[i].x+mxOff)*10;
    _setColor(xx,yy,0,0,255);
    for (int ii=1;ii<=9;ii++)
      for (int jj=1;jj<=9;jj++)
        for (int cc=0;cc<3;cc++)
          otm.at<Vec3b>(xx+ii,yy+jj)[cc]=otm.at<Vec3b>(xx,yy)[cc];
  }
  int xx=(gLoc(bloc.x,bloc.z)->cdZ+mzOff)*10,yy=(gLoc(bloc.x,bloc.z)->cdX+mxOff)*10;
  _setColor(xx,yy,255,0,0);
  for (int ii=1;ii<=9;ii++)
    for (int jj=1;jj<=9;jj++)
      for (int cc=0;cc<3;cc++)
        otm.at<Vec3b>(xx+ii,yy+jj)[cc]=otm.at<Vec3b>(xx,yy)[cc];
  imwrite(fname.c_str(),otm);
  return 0;
}

bool LxMap::judgeClose(const Point &destP,const Point3d &botLoc)
{
  return (abs(destP.x-gLoc(botLoc.x,botLoc.z)->cdX)<2&&abs(destP.y-gLoc(botLoc.x,botLoc.z)->cdZ)<2);
}

