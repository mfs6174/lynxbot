#ifndef LXCONFIG_H
#define LXCONFIG_H

struct LxConf
{
  LxConf();
  bool load(string confile);
  bool save(string confile);

  int cam_dev_id[2];
  
  Size framesize;
  int exposure;
  bool autoexposure;

  float kd_search_sigma;

  float ldmk_el_error;
  float ldmk_dispa_min,ldmk_dispa_max;
  float ldmk_ori_error;
  float ldmk_des_dist_th1,ldmk_des_dist_th2;
  float ldmk_des_rat_th1,ldmk_des_rat_th2,ldmk_des_rat_th3;
  
  int cl_ppg_th;
  int cl_vldfin_th;
  float cl_lyr_len;
  int cl_pnpl_hth;

  float bot_max_cob;
  float bot_width,bot_length,bot_height,bot_cam_h;

  float map_upd_coe1;
  float map_upd_coe2;

  int ms_max_gpt,ms_maxlc_bfrgd;

  float vo_proj_maxerr;
};

#endif
