#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXService.h"
#include "LXCamera.h"
#include "LXGeometry.h"
#include "LXCalculate.h"
#include "LXVisualOdometry.h"

extern LxConf gloconf;
#define P2CN 0.99
#define MAXPOOLLENGTH 20000
int pooll[MAXPOOLLENGTH],pbest[MAXPOOLLENGTH];

inline Mat _Point2QuadMat(Point3d p,int lr)//转换为四元数矩阵(参考[Horn1956])
{
  Mat r=Mat::zeros(4,4,CV_64F);
  if (lr==1)
  {
    r.at<double>(1,0)=r.at<double>(2,3)=p.x;
    r.at<double>(2,0)=r.at<double>(3,1)=p.y;
    r.at<double>(3,0)=r.at<double>(1,2)=p.z;
    r.at<double>(0,1)=r.at<double>(3,2)=-p.x;
    r.at<double>(0,2)=r.at<double>(1,3)=-p.y;
    r.at<double>(0,3)=r.at<double>(2,1)=-p.z;
  }
  else
  {
    r.at<double>(1,0)=r.at<double>(3,2)=p.x;
    r.at<double>(2,0)=r.at<double>(1,3)=p.y;
    r.at<double>(3,0)=r.at<double>(2,1)=p.z;
    r.at<double>(0,1)=r.at<double>(2,3)=-p.x;
    r.at<double>(0,2)=r.at<double>(3,1)=-p.y;
    r.at<double>(0,3)=r.at<double>(1,2)=-p.z;
  }
  return r.clone();
}
  
inline void _CalcRTLinear(vector<Point3d> &p1,vector<Point3d> &p2,int n,LxGMTrans &Rs)
  //以线性最小二乘估计运动,参考[Horn1956]
{
  Point3d wp1(0,0,0),wp2(0,0,0);
  for (int i=1;i<=n;i++)
  {
    wp1+=p1[pooll[i]];
    wp2+=p2[pooll[i]];
  }
  wp1*=1.0/n;
  wp2*=1.0/n;//计算重心
  Mat A=Mat::zeros(4,4,CV_64F);
  for (int i=1;i<=n;i++)
    A+=( (_Point2QuadMat(p1[pooll[i]]-wp1,1).t() )*_Point2QuadMat(p2[pooll[i]]-wp2,2) );
  Mat val,vec;
  eigen(A,val,vec);
  Rs.setRfromQuater(vec.row(0));//旋转四元数即最小特征值对应的特征向量
  Rs.setT(P3d2Mat(wp2)-Rs.R*P3d2Mat(wp1));
}

inline int _CountIn(vector<Point3d> &p1,vector<Point3d> &p2,LxGMTrans &et,int pool[],double dt)
  //统计内点 et为输入的运动估计,pool为使用的缓冲区,dt为距离阈值
{
  int n=p1.size(),m=0;
  double d;
  for (int i=0;i<n;i++)
  {
    d=norm( et.trans(p1[i])-p2[i] );
    if (d<dt)
    {
      m++;
      pool[m]=i;
    }
  }
  return m;
}

int _DoRansac(vector<Point3d> &p1,vector<Point3d> &p2,LxGMTrans &rt,int &inln,double xi,int maxsample,double dt)
//xi为对外点比例的初始最悲观估计,maxsample为对采样次数的硬限制,dt为内点距离阈值(mm)
//参考<Multiple View Geometry in Computer Vision> p110附近
{
  int n=p1.size();
  if (n<10)
    return -1;
  const int s=3;//采样点数
  int maxN=log(1-P2CN)/log(1-pow(1-xi,s));
  int ct=0,tr=0,mm=0,m=0,th=std::max((1-xi)*n-3,3.0);
  LxGMTrans et;
  while (tr<maxN)
  {
    for (ct=1;ct<=maxsample;ct++)
    {
      for (int i=1;i<=s;i++)
        pooll[i]=LxRandServ::getn(n);//采样,使用静态缓冲区pooll
      _CalcRTLinear(p1,p2,3,et);
      m=_CountIn(p1,p2,et,pooll,dt);//统计内点,用pooll记录
      if (m>th)
      {
        _CalcRTLinear(p1,p2,m,et);
        m=_CountIn(p1,p2,et,pooll,dt);
        break;
      }
    } 
    if (m>mm)
    {
      mm=m;
      memcpy(pbest,pooll,(mm+1)*sizeof(int));//最佳结果存入静态缓冲区pbest
      xi=1-mm/(double)n;//更新外点比例
      maxN=log(1-P2CN)/log(1-pow(1-xi,s));//更新需要的采样次数(有P2CN的概率至少取到一次全是内点)
      //th=(1-xi)*n-3
    }
    tr++;
  }
  inln=mm;
  rt.ass(et);
  return 0;
}

vector<Point3d> *lp;
vector<Point2d> *rp1;
vector<Point2d> *rp2;
LxStereoCam* ucam;


inline double  sq(double x)
{
  return x*x;
}

inline void calcU(double X,double Y,double Z,double q0,double qx,double qy,double qz,
                  double dX,double dY,double dZ,double f,double Txf,Mat Ja)
{
  Ja.at<double>(0)=(f*(2*Y*qz - 2*Z*qy))/(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz)) - ((2*X*qy - 2*Y*qz)*(Txf + f*(dX - X*(2*sq(qy) + 2*sq(qz) - 1) - Y*(2*q0*qz - 2*qx*qy) + Z*(2*q0*qy + 2*qx*qz))))/(sq(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz)));
  Ja.at<double>(1)=((2*X*qz - 4*Z*qx)*(Txf + f*(dX - X*(2*sq(qy) + 2*sq(qz) - 1) - Y*(2*q0*qz - 2*qx*qy) + Z*(2*q0*qy + 2*qx*qz))))/sq(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz)) - (f*(2*Y*qy + 2*Z*qz))/(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
  Ja.at<double>(2)=- (f*(2*Y*qx - 4*X*qy + 2*Z*q0))/(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz)) - ((Txf + f*(dX - X*(2*sq(qy) + 2*sq(qz) - 1) - Y*(2*q0*qz - 2*qx*qy) + Z*(2*q0*qy + 2*qx*qz)))*(2*X*q0 - 2*Y*qz + 4*Z*qy))/sq(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
  Ja.at<double>(3)=((2*X*qx + Y*(2*q0 + 2*qy))*(Txf + f*(dX - X*(2*sq(qy) + 2*sq(qz) - 1) - Y*(2*q0*qz - 2*qx*qy) + Z*(2*q0*qy + 2*qx*qz))))/sq(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz)) + (f*(4*X*qz + 2*Y*q0 - 2*Z*qx))/(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
  Ja.at<double>(4)=-f/(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
  Ja.at<double>(5)=0.0;
  Ja.at<double>(6)=(Txf + f*(dX - X*(2*sq(qy) + 2*sq(qz) - 1) - Y*(2*q0*qz - 2*qx*qy) + Z*(2*q0*qy + 2*qx*qz)))/sq(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
}

inline void calcV(double X,double Y,double Z,double q0,double qx,double qy,double qz,
                  double dX,double dY,double dZ,double f,Mat Ja)
{
  Ja.at<double>(0)=- (f*(2*X*qz - 2*Z*qx))/(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz)) - (f*(2*X*qy - 2*Y*qz)*(dY - Y*(2*sq(qx) + 2*sq(qz) - 1) + X*(2*q0*qz + 2*qx*qy) - Z*(2*q0*qx - 2*qy*qz)))/sq(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
  Ja.at<double>(1)=(f*(4*Y*qx - 2*X*qy + 2*Z*q0))/(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz)) + (f*(2*X*qz - 4*Z*qx)*(dY - Y*(2*sq(qx) + 2*sq(qz) - 1) + X*(2*q0*qz + 2*qx*qy) - Z*(2*q0*qx - 2*qy*qz)))/sq(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
  Ja.at<double>(2)=- (f*(2*X*qx + 2*Z*qz))/(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz)) - (f*(2*X*q0 - 2*Y*qz + 4*Z*qy)*(dY - Y*(2*sq(qx) + 2*sq(qz) - 1) + X*(2*q0*qz + 2*qx*qy) - Z*(2*q0*qx - 2*qy*qz)))/sq(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
  Ja.at<double>(3)=(f*(2*X*qx + Y*(2*q0 + 2*qy))*(dY - Y*(2*sq(qx) + 2*sq(qz) - 1) + X*(2*q0*qz + 2*qx*qy) - Z*(2*q0*qx - 2*qy*qz)))/sq(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz)) - (f*(2*X*q0 - 4*Y*qz + 2*Z*qy))/(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
  Ja.at<double>(4)=0.0;
  Ja.at<double>(5)=-f/(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
  Ja.at<double>(6)=(f*(dY - Y*(2*sq(qx) + 2*sq(qz) - 1) + X*(2*q0*qz + 2*qx*qy)) - Z*f*(2*q0*qx - 2*qy*qz))/sq(dZ - Z*(2*sq(qx) + 2*sq(qy) - 1) - X*(2*q0*qy - 2*qx*qz) + Y*(2*q0*qz + 2*qy*qz));
}

void calcJac( const Mat &src, Mat &dst ,int n)//计算Jacobian矩阵
{
  dst.create(n*4,7,CV_64F);
  for (int i=1;i<=n;i++)
  {
    calcU( (*lp)[pbest[i]].x,(*lp)[pbest[i]].y,(*lp)[pbest[i]].z,
           src.at<double>(0),src.at<double>(1),src.at<double>(2),src.at<double>(3),
           src.at<double>(4),src.at<double>(5),src.at<double>(6),
           ucam->f,ucam->tf,dst.row(4*(i-1) )  );
    calcV( (*lp)[pbest[i]].x,(*lp)[pbest[i]].y,(*lp)[pbest[i]].z,
           src.at<double>(0),src.at<double>(1),src.at<double>(2),src.at<double>(3),
           src.at<double>(4),src.at<double>(5),src.at<double>(6),
           ucam->f,dst.row(4*(i-1)+1 )  );
    calcU( (*lp)[pbest[i]].x,(*lp)[pbest[i]].y,(*lp)[pbest[i]].z,
           src.at<double>(0),src.at<double>(1),src.at<double>(2),src.at<double>(3),
           src.at<double>(4),src.at<double>(5),src.at<double>(6),
           ucam->f,0,dst.row(4*(i-1)+2 )  );
    calcV( (*lp)[pbest[i]].x,(*lp)[pbest[i]].y,(*lp)[pbest[i]].z,
           src.at<double>(0),src.at<double>(1),src.at<double>(2),src.at<double>(3),
           src.at<double>(4),src.at<double>(5),src.at<double>(6),
           ucam->f,dst.row(4*(i-1)+3 )  );
  }
}

void calcF( const Mat &src, Mat &dst ,int n)//计算优化值f的函数,即反投影误差
{
  dst.create(n*4,1,CV_64F);
  LxGMTrans RT;
  RT.setRfromQuater(src.rowRange(0,4));
  RT.setT(src.rowRange(4,7));
  Point3d p3d;
  Point2d p2d;
  for (int i=1;i<=n;i++)
  {
    p3d=RT.trans( (*lp)[pbest[i]] );
    p2d=ucam->project(p3d,1);
    dst.at<double>(4*(i-1)  )=(*rp1)[pbest[i]].x-p2d.x;
    dst.at<double>(4*(i-1)+1)=(*rp1)[pbest[i]].y-p2d.y;
    p2d=ucam->project(p3d,2);
    dst.at<double>(4*(i-1)+2)=(*rp2)[pbest[i]].x-p2d.x;
    dst.at<double>(4*(i-1)+3)=(*rp2)[pbest[i]].y-p2d.y;
  }
}

void _GetTransbyLM(LxGMTrans &RT,int n)
{
  Mat InitX(7,1,CV_64F),ResX(7,1,CV_64F);
  RT.getParaVec(InitX);//以输入的运动RT建立优化向量的初始值
  LxLevenbergMarquardt(calcJac,calcF,InitX,ResX,30,1e-7,0.01,n);
  RT.setRfromQuater(ResX.rowRange(0,4));
  RT.setT(ResX.rowRange(4,7));
}

 int LxVOUpdate(vector<Point3d> &p1,vector<Point3d> &p2,LxGMTrans &trans,
                 vector<Point2d> &p3,vector<Point2d> &p4,LxStereoCam &cam)
{
  LxGMTrans ttrans;
  int numIn,ret;//ransac确认的内点数
  lp=&p1;rp1=&p3;rp2=&p4;ucam=&cam;
  ret=_DoRansac(p1,p2,ttrans,numIn,0.4,15,gloconf.vo_proj_maxerr);
  if (ret||numIn<4)
    return -1;
  _GetTransbyLM(ttrans,numIn);
  double yaw,roll,pitch;
  ttrans.getER(yaw,roll,pitch);
  if (abs(ttrans.T.at<double>(0))>800||abs(ttrans.T.at<double>(1))>800||abs(ttrans.T.at<double>(2))>800||abs(yaw)>45||abs(roll)>45||abs(pitch)>45)
    return -1;
  trans.ass(ttrans);
  return 0;
}
