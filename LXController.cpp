#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXService.h"
#include "LXController.h"

int Con_openport(const char *Dev)  
{
  int fd = open( Dev, O_RDWR|O_NOCTTY|O_NDELAY );
  if (-1 == fd) 
  {    
    perror("Can''t Open Serial Port");
    return -1;  
  }
  return fd;
}

int Con_setport(int fd, int baud,int databits,int stopbits,int parity)
{
  int baudrate;
  struct   termios   newtio;  
  switch(baud)
  {
  case 300:
    baudrate=B300;
    break;
  case 600:
    baudrate=B600;
    break;
  case 1200:
    baudrate=B1200;
    break;
  case 2400:
    baudrate=B2400;
    break;
  case 4800:
    baudrate=B4800;
    break;
  case 9600:
    baudrate=B9600;
    break;
  case 19200:
    baudrate=B19200;
    break;
  case 38400:
    baudrate=B38400;
    break;
  default :
    baudrate=B9600;  
    break;
  }
  tcgetattr(fd,&newtio);    
  bzero(&newtio,sizeof(newtio));  
  //setting   c_cflag
  newtio.c_cflag   &=~CSIZE;    
  switch (databits) /*设置数据位数*/
  {  
  case 7:  
    newtio.c_cflag |= CS7; //7位数据位
    break;
  case 8:    
    newtio.c_cflag |= CS8; //8位数据位
    break;  
  default:   
    newtio.c_cflag |= CS8;
    break;    
  }
  switch (parity) //设置校验
  {  
  case 'n':
  case 'N':   
    newtio.c_cflag &= ~PARENB;   /* Clear parity enable */
    newtio.c_iflag &= ~INPCK;     /* Enable parity checking */
    break; 
  case 'o':  
  case 'O':    
    newtio.c_cflag |= (PARODD | PARENB); /* 设置为奇效验*/ 
    newtio.c_iflag |= INPCK;             /* Disnable parity checking */
    break; 
  case 'e': 
  case 'E':  
    newtio.c_cflag |= PARENB;     /* Enable parity */   
    newtio.c_cflag &= ~PARODD;   /* 转换为偶效验*/    
    newtio.c_iflag |= INPCK;       /* Disnable parity checking */
    break;
  case 'S':
  case 's':  /*as no parity*/  
    newtio.c_cflag &= ~PARENB;
    newtio.c_cflag &= ~CSTOPB;break; 
  default:  
    newtio.c_cflag &= ~PARENB;   /* Clear parity enable */
    newtio.c_iflag &= ~INPCK;     /* Enable parity checking */
    break;   
  }
  switch (stopbits)//设置停止位
  {  
  case 1:   
    newtio.c_cflag &= ~CSTOPB;  //1
    break; 
  case 2:   
    newtio.c_cflag |= CSTOPB;  //2
    break;
  default: 
    newtio.c_cflag &= ~CSTOPB; 
    break; 
  }
  newtio.c_cc[VTIME] = 0;   
  newtio.c_cc[VMIN] = 0;
  newtio.c_cflag   |=   (CLOCAL|CREAD);
  newtio.c_oflag|=OPOST;
  newtio.c_iflag   &=~(IXON|IXOFF|IXANY);                    
  cfsetispeed(&newtio,baudrate);  
  cfsetospeed(&newtio,baudrate);  
  tcflush(fd,   TCIFLUSH);
  if (tcsetattr(fd,TCSANOW,&newtio) != 0)  
  {
    perror("SetupSerial 3"); 
    return -1; 
  } 
  return 0;
}

inline int Con_writeport(int fd,const char *buf,int len)  //发送数据
{
  write(fd,buf,len);
  return 0;
}

int LxController::connect(const char *dev)
{
  fd=Con_openport(dev);
  if(fd>0)
  {
    int ret=Con_setport(fd,9600,8,1,'N');  //设置串口，波特率，数据位，停止位，校验
    if(ret<0)
    {
      printf("Can't Set Serial Port!/n");
      return -1;
    }
  }
  else
  {
    printf("Can't Open Serial Port!/n");
    return -1;
  }
  return 0;
}

int LxController::moveFwd(int _time)
{
  timer.reset();
  Con_writeport(fd,ORDER,1);
  while (timer.st()<_time);
  Con_writeport(fd,ORDER+4,1);
  return 0;
}

int LxController::moveBwd(int _time)
{
  timer.reset();
  Con_writeport(fd,&ORDER[1],1);
  while (timer.st()<_time);
  Con_writeport(fd,&ORDER[4],1);
  return 0;
}

int LxController::turnRt(int _time)
{
  timer.reset();
  Con_writeport(fd,&ORDER[2],1);
  while (timer.st()<_time);
  Con_writeport(fd,&ORDER[4],1);
  return 0;

}
int LxController::turnLt(int _time)
{
  timer.reset();
  Con_writeport(fd,&ORDER[3],1);
  while (timer.st()<_time);
  Con_writeport(fd,&ORDER[4],1);
  return 0;
}

void LxController::shutdown()
{
  close(fd); 
}


