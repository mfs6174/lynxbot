#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXKDTree.h"

LxKDTree *LXKDTree_Operating;

void LxKDTree::set(int _dim,int _size)//setup a tree,alloc memory(for nodes,data points and index space),set the dimension,clear the node counter.
{
  dim=_dim;
  if (nodes.size()<_size+1)
    nodes.resize(_size+1);
  if (data.size()<_size+1)
    data.resize(_size+1);
  if (pset.size()<_size+1)
    pset.resize(_size+1);
  nd=1;
  sflag=0;
}

void LxKDTree::build(int now,int l,int r,int dep)//build a tree(after store the data point in it) (node procesing now,index bondary and the depth)
{
  sflag=nodes[now].spt=dep%dim;//set the dimension using to split in this node
  if (l==r)//if node is a leaf
  {
    nodes[now].pnt=pset[l];
    nodes[now].left=nodes[now].right=-1;
    nodes[now].mid=data[nodes[now].pnt].d[nodes[now].spt];
    return;
  }
  int t=(r-l)>>1;
  nth_element(&pset[l],&pset[l+t],&pset[r+1],cmp);//split the index space
  nodes[now].pnt=pset[l+t];//link node with its data point
  nodes[now].mid=data[nodes[now].pnt].d[nodes[now].spt];//where to split the points in the .spt dimension
  if (t>=1)//if there are points to put to node's left son
  {
    nd++;
    nodes[now].left=nd;//increase the counter and set the left son
    for (int i=0;i<MAX_RANG_DIM;i++)
    {
      nodes[nd].rect[i][0]=nodes[now].rect[i][0];
      nodes[nd].rect[i][1]=nodes[now].rect[i][1];
    }
    nodes[nd].rect[nodes[now].spt][1]=nodes[now].mid;//split the range rectangle for left son
    build(nd,l,l+t-1,dep+1);//continue to bulid the left subtree.points's whose index are in l to l+t-1 are the points whose .spt dimention is less than .mid
  }
  else
    nodes[now].left=-1;
  nd++;
  for (int i=0;i<MAX_RANG_DIM;i++)
  {
    nodes[nd].rect[i][0]=nodes[now].rect[i][0];
    nodes[nd].rect[i][1]=nodes[now].rect[i][1];
  }
  nodes[nd].rect[nodes[now].spt][0]=nodes[now].mid;
  nodes[now].right=nd;
  build(nd,l+t+1,r,dep+1);//the same with the left,build the right subtree.
}

bool LxKDTree::cmp(const int a,const int b)//must set the global pointer LXKDTree_Operating to use cmp function for STL nth_element
{
  return LXKDTree_Operating->data[a].d[LXKDTree_Operating->sflag]<LXKDTree_Operating->data[b].d[LXKDTree_Operating->sflag];
}

void LxKDTree::reportSub(int now,int lst[])//report all the points in the subtree of now to lst[]
{
  if (now==-1)
    return;
  lst[0]++;
  lst[lst[0]]=nodes[now].pnt;
  reportSub(nodes[now].left,lst);
  reportSub(nodes[now].right,lst);
}

bool LxKDTree::pointInRange(float _point[],float _range[MAX_RANG_DIM][2])
{
  bool t=true;
  for (int i=0;i<MAX_RANG_DIM;i++)
    t=t&&(_point[i]>=_range[i][0])&&(_point[i]<=_range[i][1]);
  return t;
}

int LxKDTree::rectInRange(float _rect[MAX_RANG_DIM][2],float _range[MAX_RANG_DIM][2])
{
  bool ins=true,ous=false;
  for (int i=0;i<MAX_RANG_DIM;i++)
  {
    ins=ins&&(_rect[i][0]>=_range[i][0])&&(_rect[i][1]<=_range[i][1]);
    ous=ous||(_rect[i][0]>_range[i][1])||(_rect[i][1]<_range[i][0]);
  }
  if (ins)
    return 1;
  if (ous)
    return -1;
  return 0;
}

void LxKDTree::rangeSearch(int now,float range[MAX_RANG_DIM][2],int lst[])
{
  if (now==1)
    lst[0]=0;
  if (now==-1)
    return;
  int rt=rectInRange(nodes[now].rect,range);
  if (rt==-1)//disjoint,just return
    return;
  if (rt==1)//include,report all the points in the subtree
  {
    reportSub(now,lst);
    return;
  }
  if (pointInRange(data[nodes[now].pnt].d,range))
  {
    lst[0]++;
    lst[lst[0]]=nodes[now].pnt;
  }//intersect,check this nodes' point and continue
  rangeSearch(nodes[now].left,range,lst);
  rangeSearch(nodes[now].right,range,lst);
}

void LxKDTree::upPoint(int _p,float _q[],int lst[],float &mmdist,int k)//update the point to reslut(for aknns)
{
  float dd=data[_p].dist(_q);
  if (dd>=mmdist)
    return;
  int pos=lst[0];
  while (pos>0 && data[lst[pos]].dist(_q)>dd) pos--;
  if (lst[0]<k)
    lst[0]++;
  for (int i=lst[0];i>pos+1;i--)
    lst[i]=lst[i-1];
  lst[pos+1]=_p;//just a brute force insert sort
  if (lst[0]>=k)//before you have all k points,mmdist should set to inf,otherwise it should be update to the k-th point's dist.
    //when radius search,just fix the mmdist to the radius
    mmdist=data[lst[lst[0]]].dist(_q);
}

void LxKDTree::exp2Leaf(int now,int lst[],float &mmdist,float _q[],priority_queue<_Backtracer> &Q,int k)
{
  while (nodes[now].left!=-1)//explore to leaf and put the point in path into the backtrace priority_queue
  {
    if (_q[nodes[now].spt]<=nodes[now].mid)//less than -> go left
    {
        Q.push(_Backtracer(now,nodes[now].right,nodes[now].mid-_q[nodes[now].spt]) );
        now=nodes[now].left;
    }
    else//no less than -> go right
    {
      Q.push(_Backtracer(now,nodes[now].left,_q[nodes[now].spt]-nodes[now].mid) );
      now=nodes[now].right;
    }
  }
  if (nodes[now].right==-1)//update the point when reach a leaf
  {
    upPoint(nodes[now].pnt,_q,lst,mmdist,k);
  }
  else
  {
    upPoint(nodes[now].pnt,_q,lst,mmdist,k);//the situation when a node have a right son which is a leaf and not have left son
    upPoint(nodes[nodes[now].right].pnt,_q,lst,mmdist,k);
  }
}

void LxKDTree::aknns(float _q[],int lst[],int k,float sigma)//approximately K nearest neighbour search(point,result,k,sigma)
{
  lst[0]=0;//clear the result counter
  priority_queue<_Backtracer> Q;
  float mmdist=1e200;
  exp2Leaf(1,lst,mmdist,_q,Q,k);//explore to leaf from root once
  while ( (!Q.empty()) && (Q.top().dist*Q.top().dist)<=mmdist/(1+sigma) )//when a point's other subtree is possible to have a more near point
  {
    int t=Q.top().y;
    upPoint(nodes[Q.top().x].pnt,_q,lst,mmdist,k);
    Q.pop();
    exp2Leaf(t,lst,mmdist,_q,Q,k);//update it and explore to leaf from it
  }
}

//before build the tree,you must do these things:
//set the tree's size and dimmension
//store data points in the tree,tree.data[i].d[j] is the i-th points's dim j,i is start from 1,j is start from 0
//set the index, tree.pset[i]=i,i is start form 1
//set the root's range rect which every dim can be min(all point's this dim) to max(all points's this dim)
//point  the global pointer to the tree you are to bulid

