#ifndef LXMAP_H
#define LXMAP_H

#define XDIM 900
#define ZDIM 600
#define MXOFF 420
#define MZOFF 550

struct LxMAS;

struct LxMBlock
{
  double h,deg;
  char obvStatus;
  bool expStatus;
  int obvTimes;
  bool passAble,cannotPass;
  int cdX,cdZ;
  multiset<LxMAS>::iterator openP;
  LxMBlock * ft;
  LxMBlock();
};

struct LxMAS
{
  int f,g;
  LxMBlock *p;
  bool operator <(const LxMAS x) const
  {
    return f<x.f;
  }
  LxMAS()
  {
    f=0;g=0;
    p=NULL;
  }
  LxMAS(int _g,int _h,LxMBlock * _p)
  {
    f=_g+_h;g=_g;
    p=_p;
  } 
};

struct LxMap
{
  LxMBlock mapStore[XDIM][ZDIM];
  int mxOff,mzOff;
  int xLb,xUb,zLb,zUb;
  double gridSize;
  Mat otm;
  LxMap(int xl,int xu,int zl,int zu,double gsize);
  int _getCoord(double x);
  LxMBlock* gLoc(double x,double z);
  LxMBlock* gIdx(int gx,int gz);
  void update(int bcnt,int mapX[],int mapZ[],double mapDem[],double lat);
  void updateByBot(const Point3d &sp);
  int getNearest(double bx,double bz,Point &np);
  int getPath(int gx,int gz,double bx,double bz,vector<Point> &path);
  int outputMap(const string &fname,const Point3d &bloc,const vector<Point> &_path);
  bool judgeClose(const Point &destP,const Point3d &botLoc);
private:
  queue<LxMBlock*> tQ;
  int boolMap[XDIM][ZDIM];
  int _setBoolIdx(int gx,int gz);
  int _setBoolLoc(double x,double z);
  uchar _getColor(double lat);
  void _setColor(int x,int y,uchar r,uchar g,uchar b);
  int _setCharIdx(int gx,int gz,uchar para);
  int _setCharLoc(double x,double z,uchar para);
  multiset<LxMAS> openList;
};

#endif
