#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXCamera.h"
#include "LXService.h"
#include "LXLandmark.h"
#include "LXGeometry.h"
#include "LXVisualOdometry.h"
#include "LXStereoSensing.h"
#include "LXPCloud.h"
#include "LXMap.h"
#include "LXController.h"
#include "LXMission.h"

LxConf gloconf;
time_t LxTimeServ::stime=0;

LxMission myMission;
LxGMTrans cpos;

int main()
{
  LxTimeServ::start();
  LxRandServ::start();
  gloconf.load("LynXData/settings.xml");
  cout<<"input XLowBound(right) XUpBound(left) ZLowBound(negative,front) ZupBound(positive,back) maxworkingtime"<<endl;
  int xu,xl,zu,zl,mw;
  cin>>xl>>xu>>zl>>zu>>mw;
  if (myMission.missionInit(cpos,xu*5,xl*5,zl*5,zu*5,mw))
  {
    cout<<"Init Fail"<<endl;
    return -1;
  }
  cout<<"input max loop,-1 means no limit"<<endl;
  cin>>mw;
  myMission.mainLoop(mw);
  gloconf.save("LynXData/settings.xml");
  cout<<"We running for "<<(LxTimeServ::uptime())<<" seconds"<<endl;
  return 0;
}
