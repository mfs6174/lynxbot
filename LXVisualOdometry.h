#ifndef LXVISUALODOMETRY_H
#define LXVISUALODOMETRY_H

int LxVOUpdate(vector<Point3d> &pl,vector<Point3d> &p2,LxGMTrans &trans,vector<Point2d> &p3,vector<Point2d> &p4,LxStereoCam &cam);
//通过前后的对应特征点求相对运动
//p1为运动后的摄像机坐标系下的特征点3D坐标,p2为运动前的
//p3,p4为p2对应的左右摄像机画面中的特征点2D坐标
//结果会被存入trans中
#endif
