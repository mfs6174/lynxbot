int state;
char vin;
unsigned long timer;
unsigned long maxTimeM[3]={0,5000,5000};


void setup()
{
  Serial.begin(9600);//,SERIAL_8E2);
  state=0;
}

void setState(int _sta)
{
  timer=millis();
  state=_sta;
  Serial.println(state); //print the state when it changes for debug use
}
  
void loop()
{
  vin = Serial.read(); //读入串口上的数据
  if (vin> 0)
  {  //检测串口是否有可以读入的数据
    switch (vin)
    {
    case 'F':
      if (state==0)
      {
        //do someting
        setState(1);
      }
      break;
    case 'B':
      if (state==0)
      {
        //do someting
        setState(1);
      }
      break;
    case 'R':
      if (state==0)
      {
        //do someting
        setState(2);
      }
      break;
    case 'L':
      if (state==0)
      {
        //do someting
        setState(2);
      }
      break;
    case 'S':
      if (state>0)
      {
        //do something
        setState(0);
      }
    }
  }
  if (state>0)
  {
    if (millis()-timer>maxTimeM[state])
    {
      //do someting to make the bot stop
      setState(0);
    }
  }   
}
