#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXGeometry.h"

Mat P3d2Mat(const Point3d p)
{
  return (Mat_<double>(3,1) << p.x,p.y,p.z);
}

Point3d Mat2P3d(const Mat &m)
{
  Point3d p;
  p.x=m.at<double>(0);
  p.y=m.at<double>(1);
  p.z=m.at<double>(2);
  return p;
}

LxGMTrans::LxGMTrans()
{
  R=Mat::eye(3,3,CV_64F);
  T=Mat::zeros(3,1,CV_64F);
  qw=qx=qy=qz=0.0;
}

void LxGMTrans::setR(const Mat &iR)
{
  CV_Assert(iR.size()==R.size());
  iR.copyTo(R);
}

void LxGMTrans::setR(double y,double p,double r)//this is not sure
{
  Mat Ryy=(Mat_<double>(3,3) << cos(y),0,sin(y),0,1,0,-sin(y),0,cos(y) );
  Mat Rxp=(Mat_<double>(3,3) << 1,0,0,0,cos(p),-sin(p),0,sin(p),cos(p) );
  Mat Rzr=(Mat_<double>(3,3) << cos(r),-sin(r),0,sin(r),cos(r),0,0,0,1 );
  R=Rzr*Rxp*Ryy;
}

void LxGMTrans::setT(const Mat &it)
{
  CV_Assert(it.size()==T.size());
  it.copyTo(T);
}

void LxGMTrans::setT(const Point3d &p)
{
  T.at<double>(0)=p.x;
  T.at<double>(1)=p.y;
  T.at<double>(2)=p.z;
}

Point3d LxGMTrans::trans(const Point3d &ip) const
{
  Mat p=P3d2Mat(ip);
  return Mat2P3d(R*p+T);
}

void LxGMTrans::acc(const LxGMTrans &t)
{
  T=R*t.T+T;
  R=R*t.R;

}

void LxGMTrans::getER(double &y,double &r,double &p) const //this is not sure 
{
  y=atan2(-(R.at<double>(2,0)),hypot(R.at<double>(2,1),R.at<double>(2,2)))/CV_PI*180;
  r=atan2(R.at<double>(1,0),R.at<double>(0,0))/CV_PI*180;
  p=atan2(R.at<double>(2,1),R.at<double>(2,2))/CV_PI*180;
}

void LxGMTrans::ass(const LxGMTrans &t)
{
  t.R.copyTo(R);
  t.T.copyTo(T);
  qw=t.qw;
  qx=t.qx;
  qy=t.qy;
  qz=t.qz;
}

void LxGMTrans::setRfromQuater(const Mat &q)
{
  qw=q.at<double>(0),qx=q.at<double>(1),qy=q.at<double>(2),qz=q.at<double>(3);
  R=(Mat_<double>(3,3) << 1-2*(qy*qy+qz*qz),2*(qx*qy-qw*qz),2*(qx*qz+qw*qy),2*(qy*qx+qw*qz),1-2*(qx*qx+qz*qz),2*(qy*qz-qw*qx),2*(qz*qx-qw*qy),2*(qz*qy+qw*qz),1-2*(qx*qx+qy*qy) );
}

void LxGMTrans::getParaVec(Mat &m) const
{
  m.create(7,1,CV_64F);
  m.at<double>(0)=qw;
  m.at<double>(1)=qx;
  m.at<double>(2)=qy;
  m.at<double>(3)=qz;
  m.at<double>(4)=T.at<double>(0);
  m.at<double>(5)=T.at<double>(1);
  m.at<double>(6)=T.at<double>(2);
}

Point3d LxGMTrans::getLoc() const
{
  return Mat2P3d(T);
}

