#ifndef LXSERVICE_H
#define LXSERVICE_H

struct LxDtimer
{
public:
  LxDtimer ();
  double dt();
  double st();
  void reset();
private:
  double stick;
  double ntick;
};

struct LxTimeServ
{
public:
  LxTimeServ();
  static void start();
  static llg uptime();
  llg dt();
  void reset();
private:
  static time_t stime;
  time_t ttime;
};

struct LxRandServ
{
public:
  static void start();
  static double get01();
  static llg getn(llg n);
};

#endif
