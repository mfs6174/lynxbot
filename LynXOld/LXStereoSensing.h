#ifndef LXSTEREOSENSING_H
#define LXSTEREOSENSING_H

#define MAXNov 120100
#define COLU 3
#define MAXLab 128
#define EDGEStep 100000

struct _MEdge1
{
  int u,v;
  float w;
};

struct _MEdge2
{
  int next,v;
};

struct _SegNode
{
  int y,x1,x2;
  float clr[COLU];
  int cost[MAXLab],sel[MAXLab];
};

struct _edgeList
{
  int nm;
  vector<_MEdge1> lst;

  _edgeList();
  void add(int u,int v,float w);
  void chgw(int Lmax);
  void reset();
  void sort();
};


struct LxSMatcher
{
public:
  Mat DsL,DsR;
  Mat disa;
  LxSMatcher();
  void update(Mat il,Mat ir);
private:
  int Lmax;
  int nn;
  _SegNode node[MAXNov];

  int Nov,Noe;
  int elHead[MAXNov];
  _MEdge2 elTree[MAXNov*2];
  _edgeList elst;
  void Preprocess(Mat i1,Mat i2);
  void doSeg();
  void treeCons();
};


#endif
