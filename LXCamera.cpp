#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXCamera.h"

#define BWLMTMODE 
#define BWLMTMODESWAP
//#define IFRESIZE
const string SWAPPATH="/dev/shm/";
const string SWAPER="/home/mfs6174/codes/LynXBot/LynXTools/LynXSwaper.bin";
extern LxConf gloconf;

LxCam::LxCam(int idx)
{
  if (!cap.open(idx))
  {
    cout<<"can not capture"<<endl;
    exit(0);
  }
  cap.set(CV_CAP_PROP_FRAME_WIDTH,1600);
  cap.set(CV_CAP_PROP_FRAME_HEIGHT,1200);
  cap.read(raw);
  ifud=false;
  id=idx;
}

void LxCam::setpro()
{
  if (!gloconf.autoexposure)
  {
    stringstream ids,vls;
    ids<<id;
    vls<<gloconf.exposure;
    system(("/usr/bin/v4l2-ctl -d"+ids.str()+" -c exposure_absolute="+vls.str() ).c_str());
  }
  //setexposure
}

void LxCam::init(string cmf,string disf)
{
  int flag=0;
  FileStorage fs(cmf,FileStorage::READ);
  flag=fs.isOpened();
  fs["Intrinsics"]>>cmo;
  if (disf.size()>0)
  {
    fs.release();
    fs.open(disf,FileStorage::READ);
    flag=flag&&fs.isOpened();
  }
  fs["Distortion"]>>dist;
  cmn=getOptimalNewCameraMatrix(cmo,dist,gloconf.framesize,0);
  initUndistortRectifyMap(cmo,dist,Mat(),cmn,gloconf.framesize,CV_16SC2,rmapx,rmapy);
  ifud=(bool)flag;
  // cout<<cmn<<endl;
}

void LxCam::cgrab()
{
  cap.read(raw);
}

void LxCam::cresize()
{
  resize(raw,frame,gloconf.framesize);
}

void LxCam::cremap()
{
  if (!ifud)
    nframe=frame.clone();
  else
    remap(frame,nframe,rmapx,rmapy,CV_INTER_LINEAR);
}

void LxCam::refresh(Mat &m)
{
  cgrab();
  cresize();
  cremap();
  m=nframe;
}

LxStereoCam::LxStereoCam(int idx1,int idx2)
{
#ifdef BWLMTMODE
  
#else
  if ( (!cap1.open(idx1)) || (!cap2.open(idx2)) )
  {
    cout<<"can not capture"<<endl;
    exit(0);
  }
  cap1.set(CV_CAP_PROP_FRAME_WIDTH,1600);
  cap1.set(CV_CAP_PROP_FRAME_HEIGHT,1200);
  cap2.set(CV_CAP_PROP_FRAME_WIDTH,1600);
  cap2.set(CV_CAP_PROP_FRAME_HEIGHT,1200);
  cap1.read(raw1);
  cap2.read(raw2);
#endif
  ifr=false;
  id1=idx1;id2=idx2;
  f=600;cx1=cx2=400;cy=300;tf=80*f;t1=-1/f;dt=0.0;
}

void LxStereoCam::setpro()
{
  if (!gloconf.autoexposure)
  {
    stringstream ids,vls,ids2;
    ids<<id1;
    vls<<gloconf.exposure;
    system(( "/usr/bin/v4l2-ctl -d"+ids.str()+" -c exposure_absolute="+vls.str() ).c_str());
    ids2<<id2;
    system( ("/usr/bin/v4l2-ctl -d"+ids2.str()+" -c exposure_absolute="+vls.str() ).c_str() );
  }
  //setexposure
}
  
void LxStereoCam::init(const string &camf1,const string &camf2,const string &rf)
{
  int flag=0;
  FileStorage fs(camf1,FileStorage::READ);
  flag=fs.isOpened();
  fs["Intrinsics"]>>cmo1;
  fs["Distortion"]>>dist1;
  fs.release();
  fs.open(camf2,FileStorage::READ);
  flag=flag&&fs.isOpened();
  fs["Intrinsics"]>>cmo2;
  fs["Distortion"]>>dist2;
  Mat R,T,R1,R2;
  fs.release();
  fs.open(rf,FileStorage::READ);
  flag=flag&&fs.isOpened();
  fs["R"]>>R;
  fs["T"]>>T;
  fs.release();
  stereoRectify(cmo1,dist1,cmo2,dist2,gloconf.framesize,
                R,T,R1,R2,_p1,_p2,_q,//0,0);
                  CALIB_ZERO_DISPARITY,0);
  initUndistortRectifyMap(cmo1,dist1,R1,_p1,gloconf.framesize,CV_16SC2,rmapx1,rmapy1);
  initUndistortRectifyMap(cmo2,dist2,R2,_p2,gloconf.framesize,CV_16SC2,rmapx2,rmapy2);
  f=_p1.at<double>(0,0);
  cx1=_p1.at<double>(0,2);
  cx2=_p2.at<double>(0,2);
  cy=_p1.at<double>(1,2);
  tf=_p2.at<double>(0,3);
  t1=_q.at<double>(3,2);//=-_q.at<double>(3,2);
  dt=_q.at<double>(3,3);
  ifr=(bool)flag;
}

void LxStereoCam::cgrab()
{
#ifdef BWLMTMODE
#ifdef BWLMTMODESWAP
  stringstream nm1,nm2;
  nm1<<id1;nm2<<id2;
  system( (SWAPER+" "+nm1.str()+" "+nm2.str()+" 2>>/dev/null").c_str() );
  raw1=imread( (SWAPPATH+"cam1.pgm"));
  raw2=imread( (SWAPPATH+"cam2.pgm"));
#else
  cap1.open(id1);
  cap1.set(CV_CAP_PROP_FRAME_WIDTH,1600);
  cap1.set(CV_CAP_PROP_FRAME_HEIGHT,1200);
  cap1.grab();
  cap1.retrieve(frame1);
  raw1=frame1.clone();
  cap1.~VideoCapture();
  frame1.release();
  cap2.open(id2);
  cap2.set(CV_CAP_PROP_FRAME_WIDTH,1600);
  cap2.set(CV_CAP_PROP_FRAME_HEIGHT,1200);
  cap2.grab();
  cap2.retrieve(frame2);
  raw2=frame2.clone();
  cap2.~VideoCapture();
  frame2.release();
#endif
#else
  cap1.grab();cap2.grab();
  if (cap1.retrieve(raw1))
  {
    //error handle
  }
  if (cap2.retrieve(raw2))
  {
    //error handle
  }
#endif
}

void LxStereoCam::cresize()
{
  resize(raw1,frame1,gloconf.framesize);
  resize(raw2,frame2,gloconf.framesize);    
}

void LxStereoCam::cremap()
{
  if (!ifr)
  {
    frame1.copyTo(nframe1);
    frame2.copyTo(nframe2);
  }
  else
  {
    remap(frame1,nframe1,rmapx1,rmapy1,INTER_LINEAR);
    remap(frame2,nframe2,rmapx2,rmapy2,INTER_LINEAR);
  }
}

void LxStereoCam::refresh(Mat &m1,Mat &m2)
{
  cgrab();
#ifdef IFRESIZE
  cresize();
#else
  frame1=raw1;
  frame2=raw2;
#endif
  cremap();
  nframe1.copyTo(m1);
  nframe2.copyTo(m2);
}

Point2d LxStereoCam::project(Point3d p,int LR) const
{
  if (LR==2)
    return Point2d( (p.x*f+tf)/p.z+cx2, p.y*f/p.z+cy );
  else
    return Point2d( p.x*f/p.z+cx1 , p.y*f/p.z+cy);
}

Point3d LxStereoCam::reproject(Point2d p,double dis) const
{
  double w=dis*t1+dt;
  return Point3d( (p.x-cx1)/w , (p.y-cy)/w,f/w);
}

void LxStereoCam::reproject(const Mat &dis,Mat &_3dmap) const
{
  if (ifr)
    reprojectImageTo3D(dis,_3dmap,_q,true);
}

