GPP=g++ -g -pg -Wall `pkg-config --libs --cflags opencv` -msse -msse2 -msse3 -o
All : main.o config.o camera.o service.o geometry.o calculate.o visualOdometry.o kdTree.o landmark.o stereoSensing.o pointCloud.o map.o controller.o mission.o
	make libs
	$(GPP)  LynXBot main.o config.o camera.o service.o geometry.o calculate.o visualOdometry.o kdTree.o landmark.o stereoSensing.o LynXTplib/libelas.a pointCloud.o map.o controller.o mission.o
main.o :LXMain.cpp LXMission.h LXConfig.h LXCommonHeader.h LXService.h LXCamera.h LXLandmark.h LXGeometry.h LXVisualOdometry.h LXStereoSensing.h LXMap.h LXController.h
	$(GPP)  main.o -c LXMain.cpp
config.o: LXConfig.cpp LXConfig.h
	$(GPP)  config.o -c LXConfig.cpp
camera.o: LXCamera.cpp LXCamera.h 
	$(GPP)  camera.o -c LXCamera.cpp
service.o: LXService.cpp LXService.h
	$(GPP)  service.o -c LXService.cpp
geometry.o: LXGeometry.cpp LXGeometry.h
	$(GPP)  geometry.o -c LXGeometry.cpp
calculate.o: LXCalculate.cpp LXCalculate.h
	$(GPP)  calculate.o -c  LXCalculate.cpp
visualOdometry.o: LXVisualOdometry.cpp LXVisualOdometry.h LXConfig.h LXCamera.h LXCommonHeader.h LXGeometry.h
	$(GPP)  visualOdometry.o -c  LXVisualOdometry.cpp
kdTree.o: LXKDTree.cpp LXKDTree.h
	$(GPP)  kdTree.o -c  LXKDTree.cpp
landmark.o: LXLandmark.cpp LXLandmark.h LXKDTree.h LXConfig.h LXCamera.h LXCommonHeader.h
	$(GPP)  landmark.o -c  LXLandmark.cpp
stereoSensing.o: LXStereoSensing.cpp LXStereoSensing.h LynXTplib/libelas.a
	$(GPP) stereoSensing.o -c LXStereoSensing.cpp
pointCloud.o: LXPCloud.cpp LXPCloud.h LXConfig.h LXCamera.h
	$(GPP) pointCloud.o -c LXPCloud.cpp
map.o: LXMap.cpp LXMap.h LXConfig.h LXCommonHeader.h
	$(GPP) map.o -c LXMap.cpp
controller.o: LXController.cpp LXController.h LXConfig.h LXCommonHeader.h LXService.h
	$(GPP) controller.o -c LXController.cpp
mission.o: LXMission.cpp LXMission.h LXConfig.h LXCommonHeader.h LXService.h LXCamera.h LXLandmark.h LXGeometry.h LXVisualOdometry.h LXStereoSensing.h LXMap.h LXController.h
	$(GPP) mission.o -c LXMission.cpp

clean:
	rm -rf *.o LynXBot
tools:
	make -C LynXTools
libs:
	make -C LynXTplib
