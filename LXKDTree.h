#ifndef LXKDTREE_H
#define LXKDTREE_H

#define MAX_DIM 128
#define MAX_RANG_DIM 2
struct _KDNode
{
  int pnt,spt;
  float mid;
  int left,right;
  float rect[MAX_RANG_DIM][2];
};

struct _KDPoint
{
  float d[MAX_DIM];
  int dim;
  float dist(float _q[]) const
  {
    float t=0;
    for (int i=0;i<dim;i++)
      t+=(d[i]-_q[i])*(d[i]-_q[i]);
    return t;
  }
};

struct _Backtracer
{
  int x,y;
  float dist;
  _Backtracer()
  {
    x=y=-1;
    dist=0;
  }
  _Backtracer(int _x,int _y,float _dist)
  {
    x=_x;y=_y;
    dist=_dist;
  }
  bool operator <(const _Backtracer &a) const
  {
    return dist>a.dist;
  }
};

struct LxKDTree
{
  vector<_KDNode> nodes;
  int nd,dim,sflag;
  vector<_KDPoint> data;
  vector<int> pset;
  static bool cmp(const int a,const int b);
  bool pointInRange(float _point[],float _range[MAX_RANG_DIM][2]);
  int rectInRange(float _rect[MAX_RANG_DIM][2],float _range[MAX_RANG_DIM][2]);
  void reportSub(int now,int lst[]);
  void upPoint(int _p,float _q[],int lst[],float &mmdist,int k);
  void set(int _dim,int _size);
  void build(int now,int l,int r,int dep);
  void rangeSearch(int now,float range[MAX_RANG_DIM][2],int lst[]);
  void exp2Leaf(int now,int lst[],float &mmdist,float _q[],priority_queue<_Backtracer> &Q,int k);
  void aknns(float _q[],int lst[],int k,float sigma);
};

#endif
