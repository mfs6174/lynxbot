#ifndef LXCONTROLLER_H
#define LXCONTROLLER_H

const char ORDER[]="FBRLS";

struct LxController
{
public:
  int connect(const char *dev);
  int moveFwd(int _time);
  int moveBwd(int _time);
  int turnRt(int _time);
  int turnLt(int _time);
  void shutdown();
private:
  int fd;
  LxDtimer timer;
  
};


#endif
