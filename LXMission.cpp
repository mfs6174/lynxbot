#include "LXCommonHeader.h"
#include "LXConfig.h"
#include "LXCamera.h"
#include "LXService.h"
#include "LXLandmark.h"
#include "LXGeometry.h"
#include "LXVisualOdometry.h"
#include "LXStereoSensing.h"
#include "LXPCloud.h"
#include "LXMap.h"
#include "LXController.h"
#include "LXMission.h"

const int mysize=200;

int hdTheta[4][4]={{45,90,135},{0,0,180},{-45,-90,-135}};


extern LxConf gloconf;

inline float _getDir(float heading,float theta,bool &dirFlag)
{
  float t=theta-heading,c,cc;
  if (t>=0)
  {
    c=t;cc=360-t;
  }
  else
  {
    cc=-t;c=360-cc;
  }
  if (heading<=0&&theta>0)
  {
    cc=360-t;
  }
  else
  {
    if (heading>0&&theta<=0)
    {
      c=360+t;
    }
  }
  if (c<cc)
  {
    dirFlag=true;
    return c;
  }
  else
  {
    dirFlag=false;
    return cc;
  }
}
  

int LxMission::missionInit(const LxGMTrans &_cpos,int _xl,int _xu,int _zl,int _zu,int _mwt)
{
  botW=gloconf.bot_width;
  botL=gloconf.bot_length;
  botH=gloconf.bot_height;
  botPos=LxGMTrans();
  //camPos=LxGMTrans();
  //camPos.setT(Point3d(0,gloconf.bot_cam_h,0));
  camPos.ass(_cpos);
  xLb=_xl;xUb=_xu;zLb=_zl;zUb=_zu;
  gpTimes=mvTimes=0;
  maxWorkingTime=_mwt;
  rgpFlag=rgdFlag=mvFlag=trFlag=sysFailFlag=homingFlag=voState=false;
  pathIdx=0;
  curPath.clear();
  traceRec.clear();
  lastDest=Point(0,0);
  scam=new LxStereoCam(gloconf.cam_dev_id[0],gloconf.cam_dev_id[1]);
  stes=new LxStereoSensor(gloconf.framesize);
  mmap=new LxMap(xLb,xUb,zLb,zUb,mysize);
  ppcl=new LxPointCLoud(mysize);
  int ret=ctl.connect("/dev/ttyUSB0");
  if (ret)
    return ret;
  scam->refresh(framebuf1,framebuf2);
  cvtColor(framebuf1,gframe1,CV_RGB2GRAY);
  cvtColor(framebuf2,gframe2,CV_RGB2GRAY);
  if ((ret=stes->LxSSProcess(gframe1,gframe2)))
  {
    cout<<"frame fail"<<endl;
    return ret;
  }
  if ((ret=ppcl->acceptData(stes->disp,camPos,botPos,scam)))
  {
    cout<<"point cloud fail"<<endl;
    return ret;
  }
  fof.init(*scam,framebuf1,framebuf2);
  mmap->updateByBot(Point3d(0,0,0));
  mmap->update(ppcl->validFin,ppcl->mapX,ppcl->mapZ,ppcl->mapDEM,0.0);
  return 0;
}

inline void LxMission::parsePos()
{
  botLoc=Point3d(botPos.T.at<double>(0),botPos.T.at<double>(1),botPos.T.at<double>(2));
  botPos.getER(heading,roll,pitch);
}

inline void LxMission::doTurn(bool flag)
{
  if (flag)
    ctl.turnRt(TURNTIME);
  else
    ctl.turnLt(TURNTIME);
}
      
int LxMission::mainLoop(int maxLoop)
{
  int mret;
  while (1)
  {
    if (maxLoop>0)
      maxLoop--;
    else
    {
      if (!maxLoop)
      {
        mret=1;
        break;
      }
    }
    if (sysFailFlag)
    {
      mret=2;
      break;
    }
    if (LxTimeServ::uptime()>maxWorkingTime)
      homingFlag=true;
    int ret=0;
    parsePos();
    if (homingFlag&&(mmap->judgeClose(destP,botLoc)))
    {
      mret=0;
      break;
    }
    traceRec.push_back(Point(mmap->gLoc(botLoc.x,botLoc.z)->cdX,mmap->gLoc(botLoc.x,botLoc.z)->cdZ));
    mmap->updateByBot(botLoc);
    scam->refresh(framebuf1,framebuf2);
    cvtColor(framebuf1,gframe1,CV_RGB2GRAY);
    cvtColor(framebuf2,gframe2,CV_RGB2GRAY);
    ret=stes->LxSSProcess(gframe1,gframe2);
    ret=ret||(ppcl->acceptData(stes->disp,camPos,botPos,scam));
    if (!ret)
      mmap->update(ppcl->validFin,ppcl->mapX,ppcl->mapZ,ppcl->mapDEM,botLoc.y);
#ifdef DEBUGP
        cout<<"loop start"<<endl;
        printf("T: %.2lf %.2lf %.2lf  R: %.3lf %.3lf %.3lf\n",
               botLoc.x, botLoc.y, botLoc.z,heading,roll,pitch);
        cout<<"homeing "<<homingFlag<<endl;
        cout<<"gpTimes "<<gpTimes<<endl;
        stringstream id;
        id<<maxLoop;
        mmap->outputMap("map"+id.str()+".png",botLoc,curPath);
#endif
#ifdef MANC
        int od;
        cin>>od;
        switch (od)
        {
        case 1:
          ctl.moveFwd(MOVETIME);
          break;
        case 2:
          ctl.moveBwd(MOVETIME);
            break;
        case 3:
          ctl.turnRt(TURNTIME);
          break;
        case 4:
          ctl.turnLt(TURNTIME);
          break;
        }
        scam->refresh(framebuf1,framebuf2);
        if (!voState)
        {
          fof.update(framebuf1,framebuf2,0);
        }
        else
        {
          fof.update(framebuf1,framebuf2,1);
        }
        ret=LxVOUpdate(fof.p3d1,fof.p3d2,curTrans,fof.p2dl,fof.p2dr,*scam);
        if (ret)
        {
          cout<<"VO Fail after move"<<endl;
          switch (od)
          {
          case 2:
            ctl.moveFwd(MOVETIME);
          break;
          case 1:
            ctl.moveBwd(MOVETIME);
            break;
          case 4:
            ctl.turnRt(TURNTIME);
          break;
          case 3:
            ctl.turnLt(TURNTIME);
          break;
          }
          voState=true;
        }
        else
        {
          fof.update(framebuf1,framebuf2,2);
          botPos.acc(curTrans);
          parsePos();
        }
        continue;
#else
    //finish prepara
    loopCount++;
    if (homingFlag)
    {
      destP=Point(0,0);
    }
    else
    {
      if ( (mmap->judgeClose(destP,botLoc))||(gpTimes>gloconf.ms_max_gpt)
           ||(loopCount>gloconf.ms_maxlc_bfrgd))
      {
        lastDest=destP;
        ret=mmap->getNearest(botLoc.x,botLoc.z,destP);
        if (ret)
        {
          homingFlag=true;
          destP=Point(0,0);
        }
        gpTimes=0;
        loopCount=0;
      }
    }
    
#ifdef DEBUGP
        printf("dest %d %d\n",(int)destP.x,(int)destP.y);
#endif

    //finish get dest
    if ((curPath.size()>0)&&(!mmap->judgeClose(curPath[pathIdx],botLoc)) )
    {
      mmap->gIdx(curPath[pathIdx].x,curPath[pathIdx].y)->cannotPass=true;
    }
    if ( (homingFlag&&loopCount>gloconf.ms_maxlc_bfrgd)||(lastDest!=destP)
         ||trFlag||mvFlag
         ||( (curPath.size()>0)&&(!mmap->judgeClose(curPath[pathIdx],botLoc)) ) )
    {
      ret=mmap->getPath(destP.x,destP.y,botLoc.x,botLoc.z,curPath);
      if (ret)
      {
        gpTimes++;
#ifdef DEBUGP
        cout<<"no path"<<endl;
#endif
        continue;
      }
      else
      {
#ifdef DEBUGP
        cout<<"reset path"<<endl;
#endif
        gpTimes++;
        if (homingFlag)
          loopCount=0;
        trFlag=false;
        mvFlag=false;
        lastDest=destP;
        pathIdx=0;
      }
    }
#ifdef DEBUGP
    printf("path length is  %d  we are at %d\n",curPath.size(),pathIdx);
#endif

    //finish getpath
    if ((pathIdx+1)<(curPath.size()))
      pathIdx++;
    LxMBlock *wm=mmap->gIdx(curPath[pathIdx].x,curPath[pathIdx].y);
    if (wm->cannotPass||(abs(wm->h-botLoc.y)>gloconf.bot_max_cob) )
    {
      wm->cannotPass=true;
      mvFlag=true;
#ifdef DEBUGP
        cout<<"can not move"<<endl;
#endif
      continue;
    }
#ifdef DEBUGP
        cout<<"type some number to continue to turn"<<endl;
        int tpi;
        cin>>tpi;
#endif
    //start turn
    double theta=hdTheta[(int)(curPath[pathIdx].x-curPath[pathIdx-1].x)][(int)(curPath[pathIdx].y-curPath[pathIdx-1].y)];
    bool dirFlag;
    mvTimes=0;
    while (_getDir(heading,theta,dirFlag)>15)
    {
      if (mvTimes>15)
      {
        wm->cannotPass=true;
        trFlag=true;
        break;
      }
      mvTimes++;
      doTurn(dirFlag);
      int loseTime=1;
      scam->refresh(framebuf1,framebuf2);
      if (!voState)
        fof.update(framebuf1,framebuf2,0);
      else
        fof.update(framebuf1,framebuf2,1);
      ret=LxVOUpdate(fof.p3d1,fof.p3d2,curTrans,fof.p2dl,fof.p2dr,*scam);
      while ((!ret)&&loseTime<3)
      {
        loseTime++;
        scam->refresh(framebuf1,framebuf2);
        fof.update(framebuf1,framebuf2,1);
        ret=LxVOUpdate(fof.p3d1,fof.p3d2,curTrans,fof.p2dl,fof.p2dr,*scam);
      }
      if (ret)
      {
        wm->cannotPass=true;
        trFlag=true;
        voState=true;
        doTurn(~dirFlag);
        break;
      }
      else
      {
        fof.update(framebuf1,framebuf2,2);
        voState=false;
      }
      botPos.acc(curTrans);
      parsePos();
#ifdef DEBUGP
      printf("T: %.2lf %.2lf %.2lf  R: %.3lf %.3lf %.3lf\n",
             botLoc.x, botLoc.y, botLoc.z,heading,roll,pitch);
#endif
      cvtColor(framebuf1,gframe1,CV_RGB2GRAY);
      cvtColor(framebuf2,gframe2,CV_RGB2GRAY);
      ret=stes->LxSSProcess(gframe1,gframe2);
      if (ret)
        continue;
      ret=ppcl->acceptData(stes->disp,camPos,botPos,scam);
      if (ret)
        continue;
      mmap->update(ppcl->validFin,ppcl->mapX,ppcl->mapZ,ppcl->mapDEM,botLoc.y);
    }
#ifdef DEBUGP
        cout<<"type something to continue to move"<<endl;
        cin>>tpi;
#endif
    //start move
    if (trFlag)
      continue;
    mvTimes=0;
    do
    {
      if (mvTimes>3)
      {
        wm->cannotPass=true;
        mvFlag=true;
        break;
      }
      mvTimes++;
      ctl.moveFwd(MOVETIME);
      int loseTime=1;
      scam->refresh(framebuf1,framebuf2);
      if (!voState)
        fof.update(framebuf1,framebuf2,0);
      else
        fof.update(framebuf1,framebuf2,1);
      ret=LxVOUpdate(fof.p3d1,fof.p3d2,curTrans,fof.p2dl,fof.p2dr,*scam);
      while ((!ret)&&loseTime<3)
      {
        loseTime++;
        scam->refresh(framebuf1,framebuf2);
        fof.update(framebuf1,framebuf2,1);
        ret=LxVOUpdate(fof.p3d1,fof.p3d2,curTrans,fof.p2dl,fof.p2dr,*scam);
      }
      if (ret)
      {
        wm->cannotPass=true;
        mvFlag=true;
        ctl.moveBwd(MOVETIME);
        voState=true;
        break;
      }
      else
      {
        fof.update(framebuf1,framebuf2,2);
        voState=false;
      }
      botPos.acc(curTrans);
      parsePos();
#ifdef DEBUGP
      printf("T: %.2lf %.2lf %.2lf  R: %.3lf %.3lf %.3lf\n",
             botLoc.x, botLoc.y, botLoc.z,heading,roll,pitch);
#endif
      cvtColor(framebuf1,gframe1,CV_RGB2GRAY);
      cvtColor(framebuf2,gframe2,CV_RGB2GRAY);
      ret=stes->LxSSProcess(gframe1,gframe2);
      if (ret)
        continue;
      ret=ppcl->acceptData(stes->disp,camPos,botPos,scam);
      if (ret)
        continue;
      mmap->update(ppcl->validFin,ppcl->mapX,ppcl->mapZ,ppcl->mapDEM,botLoc.y);
    }
    while (!mmap->judgeClose(curPath[pathIdx],botLoc));
    if (mmap->judgeClose(curPath[pathIdx],botLoc) && (!mvFlag))
      pathIdx++;
#endif
  }
  printf("Finish with T: %.2lf %.2lf %.2lf  R: %.3lf %.3lf %.3lf\n",
         botLoc.x, botLoc.y, botLoc.z,heading,roll,pitch);
  mmap->outputMap("map.png",botLoc,curPath);
  ofstream recOut("trace.dat");
  for (int i=0;i<traceRec.size();i++)
    recOut<<'('<<traceRec[i].x<<' '<<traceRec[i].y<<")-->";
  recOut<<endl;
  return mret;
}

LxMission::~LxMission()
{
  delete scam;
  delete stes;
  delete mmap;
  delete ppcl;
}
